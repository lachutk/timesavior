from .views import currentUser, GetCSRFToken, UpdateUserProfileView, GetUserProfileView, DeleteAccountView
from django.urls import path

urlpatterns = [
    path('current/', currentUser.as_view()),
    path('csrf_cookie', GetCSRFToken.as_view()),
    path('update', UpdateUserProfileView.as_view()),
    path('user', GetUserProfileView.as_view()),
    path('delete', DeleteAccountView.as_view()),
    
]