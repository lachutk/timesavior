from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import UserTag, Friend, LoginHistory, Banned, TaskTag, NoteTag, Photo, Note, Task, TaskPeriodicityEdited#, PhotoNote
from .serializers import UserTagSerializer, FriendSerializer, LoginHistorySerializer, BannedSerializer, TaskTagSerializer, NoteTagSerializer, PhotoSerializer, NoteSerializer, TaskSerializer, TaskPeriodicityEditedSerializer, UserSerializer#, PhotoNoteSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Q
import django.db as db
from django.contrib.auth import get_user_model
User = get_user_model()


class UserTagCreate(generics.ListCreateAPIView):
    queryset = UserTag.objects.all()
    serializer_class = UserTagSerializer

class FriendView(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            user_friends = Friend.objects.filter(user_id=user.id)
            user_friends = FriendSerializer(user_friends, many=True)
            list_friend_names = []

            for item in user_friends.data:
                name = User.objects.values('email').get(id=item['friend_id'])
                list_friend_names.append({'friend_id': item['friend_id'], 'name': name['email']})

            return Response({ 'user_friends': list_friend_names, 'friend_relation': user_friends.data })
        except:
            return Response({ 'error': 'Something went wrong when downloading user friends' })

    def post(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            friend_email = data['friend_email']
            friend_id = User.objects.get(email=friend_email)

            f = Friend.objects.create(friend_id=friend_id, user_id=user)

            user_friend = Friend.objects.get(id=f.id)
            user_friend = FriendSerializer(user_friend)

            return Response({ 'success': 'User friend added', 'user_friend': user_friend.data })
        except:
            return Response({ 'error': 'Something went wrong when user friend was being added' })

    def delete(self, request, format=None):
        try:
            data = self.request.data
            relation_id = data['relation_id']

            Friend.objects.filter(id=relation_id).delete()

            return Response({ 'success': 'User friend delete successfully' })
        except:
            return Response({ 'error': 'Something went wrong when deleting user friend' })

class LoginHistoryCreate(generics.ListCreateAPIView):
    queryset = LoginHistory.objects.all()
    serializer_class = LoginHistorySerializer

class BannedCreate(generics.ListCreateAPIView):
    queryset = Banned.objects.all()
    serializer_class = BannedSerializer

class TaskTagView(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            user_task_tags = TaskTag.objects.filter(Q(user_id=user.id) | Q(user_id=1))
            user_task_tags = TaskTagSerializer(user_task_tags, many=True)

            return Response({ 'task_tags': user_task_tags.data })
        except:
            return Response({ 'error': 'Something went wrong when downloading task tags' })

    def post(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            tag_name = data['tag_name']
            share = data['share']

            TaskTag.objects.create(tag_name=tag_name, share=share, user_id=user)

            task_tag = TaskTag.objects.get(tag_name=tag_name)
            task_tag = TaskTagSerializer(task_tag)

            return Response({ 'taskTag': task_tag.data, 'success': 'Task tag added' })
        except:
            return Response({ 'error': 'Something went wrong when task tag was being added' })

    def delete(self, request, format=None):
        try:
            data = self.request.data
            tag_id = data['tag_id']

            TaskTag.objects.filter(id=tag_id).delete()

            return Response({ 'success': 'Task tag delete successfully' })
        except:
            return Response({ 'error': 'Something went wrong when deleting task tag' })

    def put(self, request, format=None):
        try:
            data = self.request.data
            tag_id = data['tag_id']
            tag_name = data['tag_name']
            if(data['share']):
                share=data['share']
                TaskTag.objects.filter(id=tag_id).update(tag_name=tag_name, share=share)
            else:
                TaskTag.objects.filter(id=tag_id).update(tag_name=tag_name)

            return Response({ 'success': 'Task tag edited successfully' })
        except:
            return Response({ 'error': 'Something went wrong when editing task tag' })

class NoteTagView(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            user_note_tags = NoteTag.objects.filter(Q(user_id=user.id) | Q(user_id=1))
            user_note_tags = NoteTagSerializer(user_note_tags, many=True)

            return Response({ 'note_tags': user_note_tags.data })
        except:
            return Response({ 'error': 'Something went wrong when downloading note tags' })

    def post(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            tag_name = data['tag_name']
            share = data['share']

            NoteTag.objects.create(tag_name=tag_name, share=share, user_id=user)

            note_tag = NoteTag.objects.get(tag_name=tag_name)
            note_tag = NoteTagSerializer(note_tag)

            return Response({ 'noteTag': note_tag.data, 'success': 'Note tag added' })
        except:
            return Response({ 'error': 'Something went wrong when note tag was being added' })

    def delete(self, request, format=None):
        try:
            data = self.request.data
            tag_id = data['tag_id']

            NoteTag.objects.filter(id=tag_id).delete()

            return Response({ 'success': 'Note tag delete successfully' })
        except:
            return Response({ 'error': 'Something went wrong when deleting note tag' })

    def put(self, request, format=None):
        try:
            data = self.request.data
            tag_id = data['tag_id']
            tag_name = data['tag_name']

            NoteTag.objects.filter(id=tag_id).update(tag_name=tag_name)

            return Response({ 'success': 'Note tag edited successfully' })
        except:
            return Response({ 'error': 'Something went wrong when editing note tag' })

class PhotoCreate(APIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer

class NoteView(generics.ListCreateAPIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            user_notes = Note.objects.filter(user_id=user.id)
            user_notes = NoteSerializer(user_notes, many=True)

            return Response({ 'notes': user_notes.data })
        except:
            return Response({ 'error': 'Something went wrong when downloading notes' })

    def post(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            note = data['note']
            date = data['date']
            task_id = data['task_id']
            edited_task_id = data['edited_task_id']
            note_tag_id = data['note_tag_id'] 

            if(task_id != None):
                task_id = Task.objects.get(id=task_id)
            else:
                task_id = None

            if(edited_task_id != None):
                edited_task_id = TaskPeriodicityEdited.objects.get(id=edited_task_id)
            else:
                edited_task_id = None

            if(note_tag_id != None):
                note_tag_id = NoteTag.objects.get(id=note_tag_id)
            else:
                note_tag_id = None

            Note.objects.create(task_id=task_id, edited_task_id=edited_task_id, note=note, note_tag=note_tag_id, date=date, user_id=user)
            this_note = Note.objects.get(note=note, user_id=user)
            this_note = NoteSerializer(this_note)

            return Response({ 'data': data, 'note': this_note.data, 'success': 'Note added' })
        except:
            return Response({ 'data': data, 'error': 'Something went wrong when note was being added' })

    def delete(self, request, format=None):
        try:
            data = self.request.data
            note_id = data['note_id']

            Note.objects.filter(id=note_id).delete()

            return Response({ 'success': 'Note delete successfully' })
        except:
            return Response({ 'error': 'Something went wrong when deleting note' })

    def put(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            note_id = data['id']
            note = data['note']
            task_id = data['task_id']
            edited_task_id = data['edited_task_id']
            note_tag_id = data['note_tag'] 

            if(task_id != None):
                task_id = Task.objects.get(id=task_id)
            else:
                task_id = None

            if(edited_task_id != None):
                edited_task_id = TaskPeriodicityEdited.objects.get(id=edited_task_id)
            else:
                edited_task_id = None

            if(note_tag_id != None):
                note_tag_id = NoteTag.objects.get(id=note_tag_id)
            else:
                note_tag_id = None

            Note.objects.filter(id=note_id).update(task_id=task_id, edited_task_id=edited_task_id, note=note, note_tag=note_tag_id, user_id=user)
            this_note = Note.objects.get(note=note, user_id=user)
            this_note = NoteSerializer(this_note)

            return Response({ 'data': data, 'note': this_note.data, 'success': 'Note edited successfully' })
        except:
            return Response({ 'data': data, 'error': 'Something went wrong when editing note' })

class GetUserTasksView(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            user_tasks = Task.objects.filter(user_id=user.id).order_by('-id')
            user_tasks = TaskSerializer(user_tasks, many=True)

            return Response({ 'tasks': user_tasks.data })
        except:
            return Response({ 'error': 'Something went wrong when retrieving profile' })

    def post(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            title = data['title']
            task_tag = data['task_tag']
            importance = data['importance']
            notification = data['notification']
            done = data['done']
            periodicity = data['periodicity']
            if(data['date'] != None):
                date = data['date']
            else:
                date = None
            if(task_tag != None):
                tag = TaskTag.objects.get(tag_name=task_tag)
            else:
                tag = None       
                
            thisTask = Task.objects.create(user_id=user, title=title, task_tag=tag, date=date, periodicity=periodicity, importance=importance, notification=notification, done=done)
            return Response({ 'success': 'Task added', 'data': data, 'id': thisTask.id, 'user_id': user.id })
        except:
            return Response({ 'error': 'Something went wrong when task was being added', 'data': data })

    def put(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            data = self.request.data
            task_id = data['id']
            title = data['title']
            task_tag = data['task_tag']
            importance = data['importance']
            notification = data['notification']
            done = data['done']
            periodicity = data['periodicity']
            if(data['date'] != None):
                date = data['date']
            else:
                date = None
            if(task_tag != None):
                tag = TaskTag.objects.get(tag_name=task_tag)
            else:
                tag = None       

            Task.objects.filter(id=task_id).update(title=title, task_tag=tag, date=date, periodicity=periodicity, importance=importance, notification=notification, done=done)

            return Response({ 'success': 'Task edited successfully' })
        except:
            return Response({ 'error': 'Something went wrong when editing task' })

    def delete(self, request, format=None):
        try:
            data = self.request.data
            task_id = data['task_id']

            Task.objects.filter(id=task_id).delete()

            return Response({ 'success': 'Task delete successfully', 'task_id': task_id })
        except:
            return Response({ 'error': 'Something went wrong when deleting task' })

class TaskPeriodicityEditedCreate(generics.ListCreateAPIView):
    queryset = TaskPeriodicityEdited.objects.all()
    serializer_class = TaskPeriodicityEditedSerializer

# class PhotoNoteCreate(generics.ListCreateAPIView):
#     queryset = PhotoNote.objects.all()
#     serializer_class = PhotoNoteSerializer

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class FriendTasksView(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            user = User.objects.get(id=user.id)

            user_friends = Friend.objects.filter(friend_id=user.id)
            all_tasks = []
            for user in user_friends:
                tasks = Task.objects.filter(user_id=user['user_id'])
                for task in tasks:
                    all_tasks.append({
                        'user': user['user_id'],
                        'title': task['title'],
                        'date': task['date'],
                        'periodicity': task['periodicity']
                    })
            # user_friends = FriendSerializer(user_friends, many=True)
                   
            return Response({ 'tasks': all_tasks.data })
        except:
            return Response({ 'error': 'Something went wrong when retrieving profile' })