from .views import UserTagCreate, FriendView, BannedCreate, TaskTagView, NoteTagView, PhotoCreate, NoteView, TaskPeriodicityEditedCreate, LoginHistoryCreate, UserList, GetUserTasksView, FriendTasksView#, PhotoNoteCreate
from django.urls import path

urlpatterns = [
    path('user-tag', UserTagCreate.as_view(), name="user-tag"),
    path('friends', FriendView.as_view(), name="friend"),
    # TODO: Create Admin app path('admin/banned/', BannedCreate.as_view(), name="banned"),
    path('task-tag', TaskTagView.as_view(), name="task-tag"),
    path('note-tag', NoteTagView.as_view(), name="note-tag"),
    path('photo', PhotoCreate.as_view(), name="photo"),
    path('note', NoteView.as_view(), name="note"),
    path('task-periodicity-edited', TaskPeriodicityEditedCreate.as_view(), name="task-periodicity-edited"),
    path('friend-tasks', FriendTasksView.as_view(), name="friend-tasks"),
    #path('photo-note/', PhotoNoteCreate.as_view(), name="photo_note"),
    path('login-history', LoginHistoryCreate.as_view(), name="login-history"),
    # TODO: Create admin app path('admin/users/', UserList.as_view()),
    path('task', GetUserTasksView.as_view(), name='task')
]