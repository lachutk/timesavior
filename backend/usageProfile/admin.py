from django.contrib import admin
from .models import UserTag, Friend, LoginHistory, Banned, TaskTag, NoteTag, Photo, Note, Task, TaskPeriodicityEdited#, PhotoNote

class TaskAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'title', 'task_tag', 'date', 'periodicity', 'importance', 'notification', 'done')
    list_display_links = ('user_id', 'title', 'task_tag')
    search_fields = ('title',)
    list_per_page = 25

class UserTagAdmin(admin.ModelAdmin):
    list_display = ('tag_name', 'user_id',)
    list_display_links = ('tag_name', 'user_id')
    search_fields = ('tag_name',)
    list_per_page = 25

class FriendAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'friend_id', 'user_tag')
    list_display_links = ('user_id', 'friend_id')
    list_per_page = 25

class LoginHistoryAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'date', 'ip', 'browser')
    list_display_links = ('user_id', 'date')
    search_fields = ('date',)
    list_per_page = 25

class BannedAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'date', 'time_of_ban', 'ip')
    list_display_links = ('user_id', 'date')
    search_fields = ('date',)
    list_per_page = 25

class TaskTagAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'tag_name', 'share')
    list_display_links = ('id', 'user_id', 'tag_name')
    search_fields = ('tag_name',)
    list_per_page = 25

class NoteTagAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'tag_name', 'share')
    list_display_links = ('user_id', 'tag_name')
    search_fields = ('tag_name',)
    list_per_page = 25

class PhotoAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'note_id', 'src', 'title', 'date')
    list_display_links = ('user_id', 'src', 'title')
    search_fields = ('title',)
    list_per_page = 25

class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_id', 'task_id', 'edited_task_id', 'note', 'date', 'note_tag')
    list_display_links = ('user_id', 'note')
    search_fields = ('note',)
    list_per_page = 25

class TaskPeriodicityEditedAdmin(admin.ModelAdmin):
    list_display = ('user_id', 'task_id', 'title', 'task_tag', 'date', 'importance', 'notification', 'done', 'if_destroyed')
    list_display_links = ('user_id', 'title')
    search_fields = ('title',)
    list_per_page = 25

# class PhotoNoteAdmin(admin.ModelAdmin):
#     list_display = ('photo_id', 'note_id')
#     list_display_links = ('photo_id', 'note_id')
#     search_fields = ('photo_id',)
#     list_per_page = 25

admin.site.register(UserTag, UserTagAdmin)
admin.site.register(Friend, FriendAdmin)
admin.site.register(LoginHistory, LoginHistoryAdmin)
admin.site.register(Banned, BannedAdmin)
admin.site.register(TaskTag, TaskTagAdmin)
admin.site.register(NoteTag, NoteTagAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Note, NoteAdmin)
admin.site.register(Task, TaskAdmin)
admin.site.register(TaskPeriodicityEdited, TaskPeriodicityEditedAdmin)
# admin.site.register(PhotoNote, PhotoNoteAdmin)