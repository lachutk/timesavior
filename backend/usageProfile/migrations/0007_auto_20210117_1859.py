# Generated by Django 3.1.4 on 2021-01-17 17:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('usageProfile', '0006_auto_20210117_1730'),
    ]

    operations = [
        migrations.RenameField(
            model_name='photo',
            old_name='tittle',
            new_name='title',
        ),
    ]
