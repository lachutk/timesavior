from rest_framework import serializers
from .models import UserTag, Friend, LoginHistory, Banned, TaskTag, NoteTag, Photo, Note, Task, TaskPeriodicityEdited
from django.contrib.auth import get_user_model
User = get_user_model()

class UserTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTag
        fields = "__all__"

class FriendSerializer(serializers.ModelSerializer):
    class Meta:
        model = Friend
        fields = "__all__"

class LoginHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LoginHistory
        fields = "__all__"

class BannedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banned
        fields = "__all__"

class TaskTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskTag
        fields = "__all__"

class NoteTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = NoteTag
        fields = "__all__"

class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = "__all__"

class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = "__all__"

class TaskSerializer(serializers.ModelSerializer):
    task_tag = serializers.SlugRelatedField(read_only=True, slug_field='tag_name')
    class Meta:
        model = Task
        fields = "__all__"

class TaskPeriodicityEditedSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskPeriodicityEdited
        fields = "__all__"

# class PhotoNoteSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = PhotoNote
#         fields = "__all__"

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email']

