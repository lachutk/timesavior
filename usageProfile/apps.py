from django.apps import AppConfig


class UsageprofileConfig(AppConfig):
    name = 'usageProfile'
