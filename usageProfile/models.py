from django.db import models
from django.utils import timezone
from django.contrib.auth.signals import user_logged_in
from django.utils.text import slugify
from django.contrib.auth import get_user_model
User = get_user_model()

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def login_history_receiver(sender, user, request, **kwargs):
    print('Logged')
    LoginHistory.objects.create(user_id = request.user, ip = get_client_ip(request), browser = request.META['HTTP_USER_AGENT'] )

user_logged_in.connect(login_history_receiver)

def one_day_hence():
    return timezone.now() + timezone.timedelta(days=1)

def get_image_filename(instance, filename):
    title = instance.post.title
    slug = slugify(title)
    return "post_images/%s-%s" % (slug, filename)  

class UserTag(models.Model):
    tag_name =  models.CharField(max_length=60)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.tag_name

class Friend(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    friend_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='login', default=2)
    user_tag = models.ForeignKey(UserTag, null=True, on_delete=models.SET_NULL, blank=True)

    def __str__(self):
        return str(self.user_id)

class LoginHistory(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    ip = models.GenericIPAddressField()
    browser =  models.CharField(max_length=60)

    def __str__(self):
        return str(self.user_id)

class Banned(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    time_of_ban = models.DateTimeField(default=one_day_hence)
    ip = models.GenericIPAddressField()

    def __str__(self):
        return str(self.user_id)

class TaskTag(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    tag_name = models.CharField(max_length=60, blank=True, null=True)
    share = models.BooleanField(default=False)

    def __str__(self):
        return self.tag_name
        
# class PhotoNote(models.Model):
#     photo_id = models.ForeignKey(Photo, on_delete=models.CASCADE)
#     note_id = models.ForeignKey(Note, on_delete=models.CASCADE)

#     def __str__(self):
#         return str(self.photo_id)

class Task(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    task_tag = models.ForeignKey(TaskTag, on_delete=models.SET_NULL, blank=True, null=True)
    date = models.DateTimeField('To do date', blank = True, null=True)
    periodicity = models.CharField(max_length=90, blank = True, null=True)
    importance = models.SmallIntegerField()
    notification = models.BooleanField()
    done = models.BooleanField()
     
    def __str__(self):
        return f"{self.title}, {self.user_id}"

class TaskPeriodicityEdited(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.PROTECT)
    title = models.CharField(max_length=60)
    task_tag = models.ForeignKey(TaskTag, on_delete=models.SET_NULL, blank=True, null=True)
    date = models.DateTimeField('To do date', blank = True, null=True)
    importance = models.SmallIntegerField()
    notification = models.BooleanField()
    done = models.BooleanField()
    if_destroyed = models.BooleanField()

    def __str__(self):
        return f"{self.title}, {self.user_id}"

class NoteTag(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    tag_name = models.CharField(max_length=60)
    share = models.BooleanField(default=False)

    def __str__(self):
        return self.tag_name
        
class Note(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    task_id = models.ForeignKey(Task, on_delete=models.SET_NULL, blank=True, null=True)
    edited_task_id = models.ForeignKey(TaskPeriodicityEdited, on_delete=models.SET_NULL, blank=True, null=True)
    note = models.TextField(blank=True)
    date = models.DateTimeField(default=timezone.now)
    note_tag = models.ForeignKey(NoteTag, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.title

class Photo(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    note_id = models.ForeignKey(Note, null=True, blank=True, on_delete=models.DO_NOTHING)
    src = models.ImageField(upload_to='images/%Y/%m/%d/')
    title = models.CharField(max_length=60, default="image")
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.tittle