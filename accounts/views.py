from .serializers import UserCreateSerializer
from .models import UserAccount
from rest_framework import generics, permissions
from rest_framework.response import Response
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_protect
from django.utils.decorators import method_decorator
from rest_framework.views import APIView
from django.views import View
import os
from django.http import HttpResponse, HttpResponseNotFound
import logging 
User = get_user_model()

class Assets(View):

    def get(self, _request, filename):
        path = os.path.join(os.path.dirname(__file__), 'static', filename)

        if os.path.isfile(path):
            with open(path, 'rb') as file:
                return HttpResponse(file.read(), content_type='application/javascript')
        else:
            return HttpResponseNotFound()

class currentUser(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserCreateSerializer

@method_decorator(ensure_csrf_cookie, name='dispatch')
class GetCSRFToken(APIView):
    permissions_classes = (permissions.AllowAny, )

    def get(self, request, format=None):
        return Response({ 'success': 'CSRF cookie set' })

class UpdateUserProfileView(APIView):
    def put(self, request, format=None):
        try:
            user = self.request.user
            username = user.email

            data = self.request.data
            first_name = data['first_name']
            last_name = data['last_name']

            UserAccount.objects.filter(email=user).update(first_name=first_name, last_name=last_name)

            user_profile = UserAccount.objects.get(email=user)
            user_profile = UserCreateSerializer(user_profile)

            return Response({ 'username': str(username), 'profile': user_profile.data })
        except:
            return Response({ 'error': 'Something went wrong when updating profile' })

class GetUserProfileView(APIView):
    def get(self, request, format=None):
        try:
            user = self.request.user
            username = user.email
            user = User.objects.get(id=user.id)

            user_profile = UserAccount.objects.get(email=user)
            user_profile = UserCreateSerializer(user_profile)

            return Response({ 'username': str(username), 'profile': user_profile.data })
        except:
            return Response({ 'error': 'Something went wrong when retrieving profile' })

class DeleteAccountView(APIView):
    def delete(self, request, format=None):
        try:
            user = self.request.user
            data = self.request.data
            password = data['current_password']

            if user.check_password(password):
                UserAccount.objects.filter(email=user).delete()
                return Response({ 'success': 'User deleted successfully' })
            else:
                return Response({ 'error': 'Invalid password' })
        except:
            return Response({ 'error': 'Something went wrong when deleting account' })