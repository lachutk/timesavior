import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { update_profile } from '../actions/profile';
import { delete_account } from '../actions/auth';
import { Helmet } from 'react-helmet';

const Profile = ({
    delete_account,
    update_profile,
    first_name_global,
    last_name_global,
    email_global
}) => { 
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        email: '',
    });

    const [formDataDelete, setFormDataDelete] = 
    useState({
        current_password: '',
    });

    const { first_name, last_name, email } = formData;
    const { current_password } = formDataDelete;

    useEffect(() => {
        setFormData({
            first_name: first_name_global,
            last_name: last_name_global,
            email: email_global,
        });
    }, [first_name_global]);

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });
    const onChangeDelete = e => setFormDataDelete({ ...formDataDelete, [e.target.name]: e.target.value });

    const onSubmit = e => {
        e.preventDefault();
        update_profile(first_name, last_name);
    };

    const onSubmitDelete = e => {
        e.preventDefault();
        delete_account(current_password);
    };
    return (
        <div className='container'>
            <Helmet>
                <title>Your Time Savior - Profile</title>
                <meta name='Profile' content='Profile page' />
            </Helmet>
            <h1 className='mt-3'>Welcome {`${first_name_global}`} in your profile settings</h1>
            <p className='mt-3 mb-3'>You can update profile below:</p>
            <form onSubmit={e => onSubmit(e)}>
                <div className='form-group'>
                    <label className='form-label' htmlFor='first_name'>First Name</label>
                    <input 
                        className='form-control'
                        type='text'
                        name='first_name'
                        placeholder={`${first_name_global}`}
                        onChange={e => onChange(e)}
                        value={first_name}
                    />
                </div>
                <div className='form-group'>
                    <label className='form-label mt-3' htmlFor='last_name'>Last Name</label>
                    <input 
                        className='form-control'
                        type='text'
                        name='last_name'
                        placeholder={`${last_name_global}`}
                        onChange={e => onChange(e)}
                        value={last_name}
                    />
                </div>
                <div className='form-group'>
                    <label className='form-label mt-3' for="staticEmail" htmlFor='email'>Email</label>
                    <input 
                        className='form-control'
                        id='staticEmail'
                        type='email'
                        name='email'
                        placeholder={`${email_global}`}
                        onChange={e => onChange(e)}
                        value={email}
                        disabled 
                    />
                </div>
                <button className='btn btn-primary mt-3' type='submit'>Update</button>
            </form>
            <p className='mt-5'>
                Put password, and click the button below to delete your account:
            </p>
            <form onSubmit={e => onSubmitDelete(e)}>
                <div className='form-group'>
                    <label className='form-label mt-3' htmlFor='current_password'>Password</label>
                    <input 
                        className='form-control'
                        type='password'
                        name='current_password'
                        placeholder='Password'
                        onChange={e => onChangeDelete(e)}
                    />
                </div>
                <button className='btn btn-danger' type='submit'>Delete Account</button>
            </form>
        </div>
    )
}

const mapStateToProps = state => ({
    first_name_global: state.profile.first_name,
    last_name_global: state.profile.last_name,
    email_global: state.profile.email,

});
export default connect(mapStateToProps, { 
    delete_account,
    update_profile 
})(Profile);