import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import Cookies from 'js-cookie';
import axios from 'axios';
import NoteTable from '../components/NoteTable';
import NoteForm from '../components/NoteForm';
import { SiAddthis } from 'react-icons/si';
import { GrClose } from 'react-icons/gr';
import { Link } from 'react-router-dom';

const Notes = () => {
    const [notes, setNotes] = useState([])
    const [loadingNotes, setLoadingNotes] = useState(false)
    const [loadingTasks, setLoadingTaks] = useState(false);
    const [loadingNoteTags, setLoadingNoteTags] = useState(false)
    const [noteTags, setNoteTags] = useState([])
    const [showForm, setShowForm] = useState(false);
    const [Tasks, setTasks] = useState([]);

    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    useEffect(() => {
        getNotes();
        getNoteTags();
        getTasks();
    }, []);

    const getNotes = () => {
        setLoadingNotes(true);
        axios.get(`${process.env.REACT_APP_API_URL}/profile/note`, config)
            .then(res => {
                setNotes(res.data.notes);
                setLoadingNotes(false)
        })
        .catch(err => {
            console.log(err);
            setLoadingNotes(false);
        });
    }
    const getNoteTags = () => {
        setLoadingNoteTags(true);
        axios.get(`${process.env.REACT_APP_API_URL}/profile/note-tag`, config)
            .then(res => {
                setNoteTags(res.data.note_tags);
                setLoadingNoteTags(false);
            })
            .catch(err => {
                console.log(err);
                setLoadingNoteTags(false);
            });
    }

    const getTasks = () => {
        setLoadingTaks(true);
        axios.get(`${process.env.REACT_APP_API_URL}/profile/task`, config)
            .then(res => {
                setTasks(res.data.tasks);
                setLoadingTaks(false)
        })
        .catch(err => {
            console.log(err);
            setLoadingTaks(false);
        });
    }

    return (
        <main className='container-fluid'>
            <Helmet>
                <title>Your Time Savior - Notes</title>
                <meta name='Notes' content='Notes page' />
            </Helmet>
            <div className="ml-3 text-center">
                <p><i>Całą instrukcję znajdziesz tutaj: <Link to='/'>Home</Link></i></p>
                <p><i>Pod przyciskiem "Show add note" znajduje się formularz do dowania notatek</i></p> 
                <p><i>Poniżej znajduje się tabela z notatkami. Możesz ją sortować, wyszukiwać po konkretnych frazach, oraz wykonywać różne operacje na notatkach</i></p>
            </div>
            <div className='row'>
                <div className='col-1 text-center'>
                    <button type="button" className="close show-button" aria-label="Close" onClick={() => setShowForm(!showForm)}>
                        {showForm ? 
                        <>
                            <GrClose size={48}/><div className='small-italic'>Hide add note</div>
                        </>
                        : 
                        <>
                            <SiAddthis size={48}/><div className='small-italic'>Show add note</div>
                        </>}
                    </button>
                </div>
                <div className='col-11'>
                    {showForm ? 
                    <section className='form'>
                        <NoteForm 
                            Tasks={Tasks}
                            notes={notes}
                            setNotes={setNotes}
                            noteTags={noteTags}
                            setNoteTags={setNoteTags}
                            loadingNoteTags={loadingNoteTags}
                            showForm={showForm}
                            setShowForm={setShowForm}
                        />
                    </section>
                    : ''}
                    {loadingNotes && loadingTasks && <p>Loading...</p>}
                    {!loadingNotes && !loadingTasks && (
                        <NoteTable
                            Tasks={Tasks}
                            notes={notes}
                            setNotes={setNotes}
                            noteTags={noteTags}
                            setNoteTags={setNoteTags}
                            loadingNotes={loadingNotes}
                            loadingNoteTags={loadingNoteTags}
                        />
                    )}
                </div>
            </div>
        </main>
    ) 
};

export default Notes;