import React, { useState, useEffect, useRef } from 'react';

function useInterval(callback, delay) {
    const intervalRef = React.useRef();
    const callbackRef = React.useRef(callback);
  
    useEffect(() => {
      callbackRef.current = callback;
    }, [callback]);
  
    useEffect(() => {
      if (typeof delay === 'number') {
        intervalRef.current = window.setInterval(() => callbackRef.current(), delay);

        return () => window.clearInterval(intervalRef.current);
      }
    }, [delay]);
    return intervalRef;
  }

const Pomodoro = () => {
    const [time, setTime] = useState(1800);
    const [timer, setTimer] = useState(1800);
    const [timeGo, setTimeGo] = useState(false);
    const [audioState, setAudioState] = useState(false);
    const [button, setButton] = useState('Start');
    const musicRef = useRef();
    // let audio = new Audio("media/melody.mp3")
    const changeButton = () => {
        setAudioState(false)
        setTimeGo(!timeGo)
        if(button == 'Start')
            setButton('Stop');
        else
            setButton('Start')
    }

    const intervalRef = useInterval(() => {
        if (time > 0) {
          setTime(time - 1);
        } else {
          window.clearInterval(intervalRef.current);
          setTime(timer);
          setAudioState(true)
        }
      }, !timeGo ? null : 1000);
    
    const secToHms = (s) => {
        var h = ((s - s % 3600) / 3600) % 60;
        s = s - h * 3600;
        var m = ((s - s % 60) / 60) % 60;
        s = s - m * 60;
        var s = s % 60;
        return (`${h}`.padStart(2, '0') + ':' + `${m}`.padStart(2, '0') + ':' + `${s}`.padStart(2, '0'))
      }
    const secToM = (s) => {
        var m = (s - s % 60) / 60;
        return (`${m}`.padStart(2, '0'))
    }
    
    useEffect(() => {
        audioPlayer()
    }, [audioState])

    const audioPlayer = () => {
        audioState ? musicRef.current.play() : musicRef.current.pause()
    }

    return (
        <div className='container'>
            <audio ref={musicRef} src="media/melody.mp3" />
            <div className='pomodoro'>
              <p><i>Ustaw czas, w jakim chcesz wykonywać jakąś czynność, a po tym czasie relaksująca melodyjka przypomnij Ci o przerwie</i></p>
                <h1>Pomodoro Timer</h1>
                <p className="timer__time">{secToHms(time)}</p>
                    <button className='btn__pomodoro' onClick={changeButton}>{button}</button>
                <div className="form-group centered mt-3">
                    <label className="control-label">Minutes of work</label>
                    <input disabled={timeGo ? 'disabled' : ''}
                    name="time" type="number" className="form-control centered"
                    placeholder="Minutes of work" value={secToM(timer)} 
                    onChange={
                        (e) => {
                            if(e.target.value >= 0){
                                setTimer(60*e.target.value)
                                setTime(60*e.target.value)
                            }
                        }
                    } />
                </div>
            </div>
        </div>
    ) 
};

export default Pomodoro;