import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import FriendTable from '../components/FriendTable';
import TaskTagsShareTable from '../components/TaskTagsShareTable';
import FriendTaskTable from '../components/FriendTaskTable';
import axios from 'axios';
import Cookies from 'js-cookie';

const Friends = () => {
    const [friendList, setFriendList] = useState([]);
    const [taskTags, setTaskTags] = useState([]);
    const [friendTasks, setFriendTasks] = useState([]);
    const [loadingFriends, setLoadingFriends] = useState(false);
    const [loadingTaskTags, setLoadingTaskTags] = useState(false);
    const [loadingFriendTasks, setLoadingFriendTasks] = useState(false);

    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };

    async function loadFriends() {
        setLoadingFriends(true);
        await axios.get(`${process.env.REACT_APP_API_URL}/profile/friends`, config)
            .then(res => {
                let newFriends = []
                res.data.friend_relation.map((friend, i) => {
                    newFriends.push({
                        id: friend.id,
                        friend_id: res.data.user_friends[i].friend_id,
                        friend_name: res.data.user_friends[i].name
                    })
                })
                setFriendList(newFriends);
                setLoadingFriends(false)
        })
        .catch(err => {
            console.log(err);
            setLoadingFriends(false);
        });
    }
    const getTaskTags = async () => {
        setLoadingTaskTags(true);
        await axios.get(`${process.env.REACT_APP_API_URL}/profile/task-tag`, config)
            .then(res => {
                setTaskTags(res.data.task_tags);
                setLoadingTaskTags(false);
            })
            .catch(err => {
                console.log(err);
                setLoadingTaskTags(false);
            });
    }
    const getFriendTasks = async () => {
        setLoadingFriendTasks(true);
        await axios.get(`${process.env.REACT_APP_API_URL}/profile/friend-tasks`, config)
            .then(res => {
                let friend_tasks = JSON.parse(res.data.friend_tasks)
                let new_tasks = []
                let periodicity = '';
                var i;
                friend_tasks.map((task) => {
                    periodicity = task.periodicity.replace(/'/g,'"');
                    periodicity = JSON.parse(periodicity)
                    let days = '';
                    for(i in periodicity)
                        {
                            if(periodicity[i] == 1)
                                days += i + ', ';
                        }
                    new_tasks.push({
                        date: task.date,
                        periodicity: days,
                        title: task.title,
                        user: task.user
                    })
                })
                console.log(new_tasks)
                setFriendTasks(new_tasks);
                setLoadingFriendTasks(false);
            })
            .catch(err => {
                console.log(err);
                setLoadingFriendTasks(false);
            });
    }

    useEffect(() => {
        loadFriends();
        getTaskTags();
        getFriendTasks();
    }, []);

    return (
        <div className='container-fluid'>
            <Helmet>
                <title>Your Time Savior - Friends</title>
                <meta name='Friends' content='Friends page' />
            </Helmet>
            <div className='row'>
                <div className='col-3'>
                    <p><i>Tutaj możesz dodać użytkownika, za pośrednictwem jego adresu email, z którym chcesz podzielić się swoimi zadaniami</i></p>
                    <h1>Friends</h1>
                    {loadingFriends && <p>Loading...</p>}
                    {!loadingFriends &&( 
                        <FriendTable 
                            friendList={friendList}
                            setFriendList={setFriendList}
                        />
                    )}
                </div>
                <div className='col-6'>
                    <p><i>Tutaj wyświetlają się zadania użytkowników, którzy Cię dodali</i></p>
                    <h1>Your friend tasks</h1>
                    {loadingFriendTasks && <p>Loading...</p>}
                    {!loadingFriendTasks &&( 
                        <FriendTaskTable 
                            friendTasks={friendTasks}
                            setFriendTasks={setFriendTasks}
                        />
                    )}
                </div>
                <div className='col-3'>
                    <p><i>Tutaj wybierasz, które kategorie/tagi udostępniasz dodanym użytkownikom</i></p> 
                    <h1>Task tags to share</h1>
                    {loadingTaskTags && <p>Loading...</p>}
                    {!loadingTaskTags &&( 
                        <TaskTagsShareTable 
                            taskTags={taskTags}
                            setTaskTags={setTaskTags}
                        />
                    )}
                </div>
            </div>
        </div>
    );
}

export default Friends;