import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import TaskTable from '../components/TaskTable';
import TaskForm from '../components/TaskForm';
import axios from 'axios';
import Cookies from 'js-cookie';
import { SiAddthis } from 'react-icons/si';
import { GrClose } from 'react-icons/gr';
import { BiCalendarPlus, BiCalendarMinus } from 'react-icons/bi';
import { Button, FormLabel, FormGroup } from 'react-bootstrap';
import moment from 'moment'; 
import { Link } from 'react-router-dom';

const Tasks = () => {
    const [Tasks, setTasks] = useState([]);
    const [taskTags, setTaskTags] = useState([]);
    const [Loading, setLoading] = useState(false);
    const [loadingTasks, setLoadingTaks] = useState(true);
    const [showForm, setShowForm] = useState(false);
    const [showIcal, setShowIcal] = useState(false);
    const [file, setFile] = useState();

    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    useEffect(() => {
        loadTasks();
        getTaskTags();
    }, []);

    async function loadTasks() {
        setLoadingTaks(true);
        await axios.get(`${process.env.REACT_APP_API_URL}/profile/task`, config)
            .then(res => {
                console.log(res.data.tasks);
                setTasks(res.data.tasks);
                setLoadingTaks(false)
        })
        .catch(err => {
            console.log(err);
            setLoadingTaks(false);
        });
    }

    const getTaskTags = async () => {
        setLoading(true);
        axios.get(`${process.env.REACT_APP_API_URL}/profile/task-tag`, config)
            .then(res => {
                console.log(res.data);
                setTaskTags(res.data.task_tags);
                setLoading(false);
            })
            .catch(err => {
                console.log(err);
                setLoading(false);
            });
    }

    const addFile = async (e) => {
        e.preventDefault()
        const reader = new FileReader()
        reader.onload = async (e) => { 
            const text = (e.target.result)
            const ical = require('cal-parser');
            const parsed = ical.parseString(text);
            setFile(parsed)
        };
        reader.readAsText(e.target.files[0])
    }

    const readFile = () => {
        console.log(file);
        console.log(file.events);
        let per = {"monday": 0, "tuesday": 0, "wednesday": 0, "thursday": 0, "friday": 0, "saturday": 0, "sunday": 0}
        // let newTasks = [...Tasks];
        file.events.map((task) => {
            let everything = task.summary.value + ' ' + moment(new Date(task.dtstart.value)).format('LT') + ' - ' + moment(new Date(task.dtend.value)).format('LT')
            const body = JSON.stringify({
                'withCredentials': true,
                title: everything,
                importance: 5,
                notification: false,
                done: false,
                task_tag: 'School',
                date: task.dtstart.value,
                periodicity: JSON.stringify(per),
            });
            axios.post(`${process.env.REACT_APP_API_URL}/profile/task`, body, config)
            .then(res => {
                console.log(res.data);
                setTasks([ {
                    id: res.data.id,
                    title: everything,
                    date: task.dtstart.value,
                    periodicity: JSON.stringify(per),
                    task_tag: 'School',
                    importance: 5,
                    notification: false,
                    done: false,
                    user_id: res.data.user_id
                }, ...Tasks, ])
            })
            .catch(err => {
                console.log(err)
            })
        })
        loadTasks();
    }

    return (
        <main className="container-fluid">
            <Helmet>
                <title>Your Time Savior - Tasks</title>
                <meta name='Tasks to do' content='Tasks page' />
            </Helmet>
            <div className="ml-3 text-center">
                <p><i>Całą instrukcję znajdziesz tutaj: <Link to='/'>Home</Link></i></p> 
                <p><i>Pod przyciskiem "Show add task" znajduje się formularz do dodawania zadań</i></p>
                <p><i>Pod przyciskiem "Show Icalendar menu" znajduje się formularz do dodawania zadań z pliku o formacie icalendar</i></p>
                <p><i>Poniżej znajduje się tabela z zadaniami. Możesz ją sortować, wyszukiwać po konkretnych frazach, oraz wykonywać różne operacje na zadaniach</i></p>
            </div>
            <div className='row'>
                <div className='col-1 text-center'>
                    <div className='ml-4'>
                        <button type="button" className="close show-button" aria-label="Close" onClick={() => setShowForm(!showForm)}>
                            {showForm ? 
                            <>
                                <GrClose size={48}/><div className='small-italic'>Hide add task</div>
                            </>
                            : 
                            <>
                                <SiAddthis size={48}/><div className='small-italic'>Show add task</div>
                            </>}
                        </button>
                    </div>
                    <div className='ml-4'>
                        <button type="button" className="close show-button" aria-label="Close" onClick={() => setShowIcal(!showIcal)}>
                            {showIcal ? 
                            <>
                                <BiCalendarMinus size={48}/><div className='small-italic'>Hide Icalendar menu</div>
                            </> 
                            : <>
                                <BiCalendarPlus size={48}/><div className='small-italic '>Show Icalendar menu</div>
                            </>}
                        </button>
                    </div>
                </div>
                <div className='col-11'>
                    {showForm ? 
                    <section className='form'>
                        <TaskForm 
                            Tasks={Tasks}
                            setTasks={setTasks}
                            taskTags={taskTags}
                            setTaskTags={setTaskTags}
                            Loading={Loading}
                            setLoading={setLoading}
                            showForm={showForm}
                            setShowForm={setShowForm}
                        />
                    </section>
                    : ''}
                <>
                    {showIcal ? 
                    <div className='text-center ml-3'>
                        <FormGroup>
                            <FormLabel for="customFile">Download your calendar (.ics and .ical files only)</FormLabel>
                        </FormGroup>
                        <FormGroup>
                            <input type="file" className="mb-2" accept='.ics' onChange={(e) => addFile(e)} />
                        </FormGroup>
                        <FormGroup>
                            <Button variant="primary" onClick={() => readFile()}>Send</Button>
                        </FormGroup>
                    </div>
                    : ''
                    }
                </>
                {loadingTasks && <p>Loading...</p>}
                {!loadingTasks &&(
                    <TaskTable
                        Tasks={Tasks}
                        setTasks={setTasks}
                        taskTags={taskTags}
                        setTaskTags={setTaskTags}
                        Loading={loadingTasks}
                        setLoading={setLoadingTaks}
                    />
                )}
            </div>
            </div>
        </main>
    );
};

export default Tasks;