import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { verify } from '../actions/auth';
import { Helmet } from 'react-helmet';

const Activation = ({ verify, match }) => {
    const [verified, setverified] = useState(false);

    const verify_new_account = e => {
        const uid = match.params.uid;
        const token = match.params.token;

        verify(uid, token);
        setverified(true);
    };

    if (verified)
        return <Redirect to='/' />;
    
    return (
        <div className='container mt-5'>
            <Helmet>
                <title>Your Time Savior - Activate</title>
                <meta name='Activate' content='Activate page' />
            </Helmet>
            <div className='col text-center margin_top__100'>
                <h1> Vetify your new Account</h1>
                <button onClick={verify_new_account}
                type='button'
                className='btn btn-primary'>
                    Verify me
                </button>
            </div>
        </div>
    );
};

export default connect(null, { verify })(Activation);