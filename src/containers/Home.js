import React from 'react';
import LoadingPage from '../components/LoadingPage';

const Home = () => (
    <LoadingPage />
);

export default Home;