import React, { useState, useEffect, useRef } from 'react';
import { Modal, Button, InputGroup, FormControl, Form } from 'react-bootstrap';
import Cookies from 'js-cookie';
import axios from 'axios';
import moment from 'moment'; 
import NoteTag from './NoteTag';
import ModalEditNoteTag from './ModalEditNoteTag';

const ModalAddNote = ({ email_global, addNoteShow, setAddNoteShow, addNoteOption, setAddNoteOption }) => {
    const [loadingNoteTags, setLoadingNoteTags] = useState(false)
    const [noteTags, setNoteTags] = useState([])
    const [selectedNoteOption, setSelectedNoteOption] = useState();
    const [editNoteTagShow, setEditNoteTagShow] = useState(false);
    const [editedTagOption, setEditedTagOption] = useState({ label: '', value: '' }); 
    const [noteData, setNoteData] = useState({
        id: addNoteOption,
        edited_task_id: null,
        note: '',
        date: moment().format(),
        note_tag_id: null
    })
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    const getNoteTags = async () => {
        setLoadingNoteTags(true);
        axios.get(`${process.env.REACT_APP_API_URL}/profile/note-tag`, config)
            .then(res => {
                console.log(res.data);
                setNoteTags(res.data.note_tags);
                setLoadingNoteTags(false);
            })
            .catch(err => {
                console.log(err);
                setLoadingNoteTags(false);
            });
    }
    useEffect(() => {
        getNoteTags();
    }, []);

    const addNoteOnChange = (e) => {
        let data = noteData;
        data[e.target.name] = e.target.value
        setNoteData(data)
    }
    const popUpAddNote = () => {
        setAddNoteShow(!addNoteShow);
        setNoteData({
            id: addNoteOption,
            edited_task_id: null,
            note: '',
            date: moment().format(),
            note_tag_id: null
        })
        setSelectedNoteOption('');
    };
    const addNoteToBackend = () => {
        const body = JSON.stringify({
            'withCredentials': true,
            task_id: noteData.id, 
            edited_task_id: noteData.edited_task_id,
            note: noteData.note,
            note_tag_id: noteData.note_tag_id,
            date: noteData.date,
        });
        axios.post(`${process.env.REACT_APP_API_URL}/profile/note`, body, config)
        .then(res => {
            console.log(res.data);
        }).catch(err => {
            console.log(err)
        })
        console.log(noteData)
        popUpAddNote();
    }

    return (
        <Modal show={addNoteShow} onHide={popUpAddNote}>
            {console.log(addNoteOption)}
            <Modal.Header closeButton>
                <Modal.Title>Add note</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Note</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        as="textarea"
                        name="note"
                        aria-label="With textarea"
                        aria-describedby="inputGroup-sizing-default"
                        defaultValue={noteData.note}
                        onChange={e => addNoteOnChange(e)}
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Note tag</InputGroup.Text>
                    </InputGroup.Prepend>
                    <div className='edit_tag_in_note'>
                        <NoteTag
                            email_global={email_global}
                            NoteTags={noteTags}
                            setNoteTags={setNoteTags}
                            selectedOption={selectedNoteOption}
                            setSelectedOption={setSelectedNoteOption}
                            noteData={noteData}
                            setNoteData={setNoteData}
                            editNoteTagShow={editNoteTagShow}
                            setEditNoteTagShow={setEditNoteTagShow}
                            setEditedOption={setEditedTagOption}
                            Loading={loadingNoteTags} 
                        />
                    </div>
                    <ModalEditNoteTag 
                        editedOption={editedTagOption}
                        setEditedOption={setEditedTagOption}
                        editNoteTagShow={editNoteTagShow} 
                        setEditNoteTagShow={setEditNoteTagShow} 
                        selectedOption={selectedNoteOption} 
                        setSelectedOption={setSelectedNoteOption} 
                        noteTags={noteTags} 
                        setNoteTags={setNoteTags}
                    />
                </InputGroup>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={popUpAddNote}>Close</Button>
                <Button variant="primary" onClick={() => addNoteToBackend()}>Add note</Button>
            </Modal.Footer>

        </Modal>
    );
}

export default ModalAddNote;