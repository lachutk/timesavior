import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import axios from 'axios';
import Cookies from 'js-cookie';
import NoteTag from './NoteTag';
import ModalEditNoteTag from './ModalEditNoteTag';
import { connect } from 'react-redux';
import moment from 'moment'; 

const NoteEditForm = ({ email_global, thisNote, Tasks, notes, setNotes, noteTags, setNoteTags, loadingNoteTags, showEditNoteForm, setShowEditNoteForm }) => {
    const [editData, setEditData] = useState({
        task_id: notes[thisNote.id].task_id,
        edited_task_id: notes[thisNote.id].edited_task_id,
        note: notes[thisNote.id].note,
        note_tag_id: notes[thisNote.id].note_tag
    });
    const [editNoteTagShow, setEditNoteTagShow] = useState(false);
    const [selectedNoteOption, setSelectedNoteOption] = useState();
    const [selectedTaskOption, setSelectedTaskOption] = useState({ label: '', value: ''});
    const [editedTagOption, setEditedTagOption] = useState({ label: '', value: '' });
   

    useEffect(() => {
        console.log(thisNote)
        if(thisNote.task != "") 
        setSelectedTaskOption({ label: thisNote.task.title, value: thisNote.task.id });
        if(thisNote.tag != "")  
            setEditedTagOption({ label: thisNote.tag.tag_name, value: thisNote.tag.id });
    }, [thisNote])

    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    // ###########NOTE##########
    const onChange = e => setEditData({ ...editData, [e.target.name]: e.target.value });
    const onChangeTask = (newValue: any, actionMeta: any) => {
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
        setSelectedTaskOption({ value: newValue.value, label: newValue.label });
        setEditData({ ...editData, task_id: newValue.value })
    }
     
    const onSubmit = e => {
        e.preventDefault();
        const body = JSON.stringify({
            'withCredentials': true,
            id: thisNote.note.id,
            task_id: editData.task_id, 
            edited_task_id: editData.edited_task_id,
            note: editData.note,
            note_tag: editData.note_tag_id
        });
        axios.put(`${process.env.REACT_APP_API_URL}/profile/note`, body, config)
        .then(res => {
            console.log(res.data);
            let newNotes = [...notes];
                newNotes[thisNote.id] = {
                    id: res.data.note.id,
                    task_id: res.data.note.task_id, 
                    edited_task_id: res.data.note.edited_task_id,
                    note: res.data.note.note,
                    note_tag: res.data.note.note_tag
                };
            setNotes(newNotes);
            setShowEditNoteForm(!showEditNoteForm);
        })
        .catch(err => {
            console.log(err)
            setShowEditNoteForm(!showEditNoteForm);
        })
    };

    return (
        <div className='container'>
            {console.log(selectedTaskOption)}
            <ModalEditNoteTag 
                editedOption={editedTagOption}
                setEditedOption={setEditedTagOption}
                editNoteTagShow={editNoteTagShow} 
                setEditNoteTagShow={setEditNoteTagShow} 
                selectedOption={selectedNoteOption} 
                setSelectedOption={setSelectedNoteOption} 
                noteTags={noteTags} 
                setNoteTags={setNoteTags}
            />
            <form onSubmit={e => onSubmit(e)}>
                <div className='form-group'>
                    <label className='form-label' htmlFor='note'>Note</label>
                    <textarea  className='form-control' placeholder='Note' name='note' value={editData.note} onChange={e => onChange(e)} required rows="6"></textarea>
                </div>
                <div className='form-group'>
                    <label className='form-label' htmlFor='note_tag'>Note tag</label>
                    <NoteTag
                        email_global={email_global}
                        NoteTags={noteTags}
                        setNoteTags={setNoteTags}
                        selectedOption={editedTagOption}
                        setSelectedOption={setEditedTagOption}
                        noteData={editData}
                        setNoteData={setEditData}
                        editNoteTagShow={editNoteTagShow}
                        setEditNoteTagShow={setEditNoteTagShow}
                        setEditedOption={setSelectedNoteOption}
                        Loading={loadingNoteTags} 
                    />
                </div>               
                <div className='form-group'>
                    <label className='form-label' htmlFor='task'>Task</label>
                    <Select 
                        placeholder='Chose task'
                        value={selectedTaskOption}
                        onChange={onChangeTask}
                        options = {Tasks.map((task) => {
                            return { value: task.id, label: task.title};
                        })} />
                </div>
                <div className='form-group'>
                    <button className='btn btn-primary' type='submit'>Ok</button>
                </div>
            </form>
        </div>
    )
};
const mapStateToProps = state => ({
    email_global: state.profile.email,

});
export default connect(mapStateToProps, )(NoteEditForm);