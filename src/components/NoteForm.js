import React, { useState } from 'react';
import Select from 'react-select';
import axios from 'axios';
import Cookies from 'js-cookie';
import NoteTag from './NoteTag';
import ModalEditNoteTag from './ModalEditNoteTag';
import { connect } from 'react-redux';
import moment from 'moment'; 
import { setAlert } from '../actions/alert';
 
const NoteForm = ({ email_global, setAlert, Tasks, notes, setNotes, noteTags, setNoteTags, loadingNoteTags, showForm, setShowForm }) => {

    const [formData, setFormData] = useState({
        task_id: null,
        edited_task_id: null,
        note: '',
        date: moment().format(),
        note_tag_id: null
    });
    const [editNoteTagShow, setEditNoteTagShow] = useState(false);
    const [selectedNoteOption, setSelectedNoteOption] = useState();
    const [selectedTaskOption, setSelectedTaskOption] = useState({ label: '', value: '' });
    const [editedTagOption, setEditedTagOption] = useState({ label: '', value: '' });

    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    // ###########NOTE##########
    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });
    const onChangeTask = (newValue: any, actionMeta: any) => {
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
        setSelectedTaskOption({ value: newValue.value, label: newValue.label });
        setFormData({ ...formData, task_id: newValue.value })
    }
    
    const setEmptyForm = () => {
        setFormData({
            task_id: null,
            edited_task_id: null,
            note: '',
            date: moment().format(),
            note_tag_id: null
        });
    }

    const onSubmit = e => {
        e.preventDefault();
        console.log(formData);
        const body = JSON.stringify({
            'withCredentials': true,
            task_id: formData.task_id, 
            edited_task_id: formData.edited_task_id,
            note: formData.note,
            note_tag_id: formData.note_tag_id,
            date: formData.date,
        });
        axios.post(`${process.env.REACT_APP_API_URL}/profile/note`, body, config)
        .then(res => {
            console.log(res.data);
            setNotes([ {
                task_id: formData.task_id, 
                edited_task_id: formData.edited_task_id,
                note: formData.note,
                note_tag: formData.note_tag_id,
                date: formData.date,
            }, ...notes, ]);
            setShowForm(!showForm);
            setEmptyForm();
            setAlert('Note added successfully', 'success');
        })
        .catch(err => {
            console.log(err)
            setShowForm(!showForm);
            setEmptyForm();
            setAlert('Note added failed', 'error');
        })
    };

    return (
        <div className='container'>
            <ModalEditNoteTag 
                editedOption={editedTagOption}
                setEditedOption={setEditedTagOption}
                editNoteTagShow={editNoteTagShow} 
                setEditNoteTagShow={setEditNoteTagShow} 
                selectedOption={selectedNoteOption} 
                setSelectedOption={setSelectedNoteOption} 
                noteTags={noteTags} 
                setNoteTags={setNoteTags}
            />
            <form onSubmit={e => onSubmit(e)}>
                <div className='form-group'>
                    <label className='form-label' htmlFor='note'>Note</label>
                    <textarea  className='form-control' placeholder='Note' name='note' value={formData.note} onChange={e => onChange(e)} required rows="6"></textarea>
                </div>
                <div className='form-group'>
                    <label className='form-label' htmlFor='note_tag'>Note tag</label>
                    <NoteTag
                        email_global={email_global}
                        NoteTags={noteTags}
                        setNoteTags={setNoteTags}
                        selectedOption={selectedNoteOption}
                        setSelectedOption={setSelectedNoteOption}
                        noteData={formData}
                        setNoteData={setFormData}
                        editNoteTagShow={editNoteTagShow}
                        setEditNoteTagShow={setEditNoteTagShow}
                        setEditedOption={setEditedTagOption}
                        Loading={loadingNoteTags} 
                    />
                </div>               
                <div className='form-group'>
                    <label className='form-label' htmlFor='task'>Task</label>
                    <Select 
                        value={selectedTaskOption}
                        onChange={onChangeTask}
                        options = {Tasks.map((task) => {
                            return { value: task.id, label: task.title};
                        })} />
                </div>
                <div className='form-group'>
                    <button className='btn btn-primary' type='submit'>Add note</button>
                </div>
            </form>
        </div>
    )
};
const mapStateToProps = state => ({
    email_global: state.profile.email,

});
export default connect(mapStateToProps, { setAlert })(NoteForm);