import { Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import React from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

const ModalTaskTagEdit = ({ editedOption, setEditedOption, editTaskTagShow, setEditTaskTagShow, selectedOption, setSelectedOption, taskTags, setTaskTags }) => {

    const popUpEditTag = (value, label) => {
        setEditTaskTagShow(!editTaskTagShow);
        setEditedOption({ value: value, label: label });
    };

    const editTaskTagToBackend = () => {
        console.group('Edited option and to what edit');
        console.log(editedOption);
        console.groupEnd();
        const body = JSON.stringify({
            'withCredentials': true,
            tag_id: editedOption.value,
            tag_name: editedOption.label
        });
        const editConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        axios.put(`${process.env.REACT_APP_API_URL}/profile/task-tag`, body, editConfig)
            .then(res => {
                setSelectedOption({ value: editedOption.value, label: editedOption.label });
                let newTaskTags = [...taskTags];
                let thisTag = newTaskTags.find((taskTag) => taskTag.id === editedOption.value);
                let index = newTaskTags.findIndex((taskTag) => taskTag.id === editedOption.value);
                newTaskTags[index] = {
                    id: editedOption.value,
                    tag_name: editedOption.label,
                    share: thisTag.share,
                    user_id: thisTag.user_id
                };
                setTaskTags(newTaskTags);
                setEditTaskTagShow(!editTaskTagShow);
            }).catch(err => {
                console.log(err)
                setEditTaskTagShow(!editTaskTagShow);
            })
    };

    return (
        <Modal show={editTaskTagShow} onHide={popUpEditTag}>

            <Modal.Header closeButton>
                <Modal.Title>Change tasks tag name</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Task tag name</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        aria-label="Default"
                        aria-describedby="inputGroup-sizing-default"
                        defaultValue={editedOption.label}
                        onChange={e => setEditedOption({ value: editedOption.value, label: e.target.value })} 
                        required    
                    />
                </InputGroup>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={popUpEditTag}>Close</Button>
                <Button variant="primary" onClick={() => editTaskTagToBackend()}>Ok</Button>
            </Modal.Footer>

        </Modal>
    );
}

export default ModalTaskTagEdit;