import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
    <div class="page-wrap d-flex flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <span class="display-1 d-block">404 Not Found</span>
                    <div class="mb-4 lead">The page you are looking for was not found.</div>
                    <Link className='btn btn-primary btn-lg mr-2' to='/' role='button'>Back to Home</Link>
                </div>
            </div>
        </div>
    </div>
);

export default NotFound;