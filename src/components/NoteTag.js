import { components } from 'react-select';
import { FiEdit } from "react-icons/fi";
import React, { Fragment } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import CreatableSelect from 'react-select/creatable';

const NoteTag = ({ email_global, NoteTags, setNoteTags, selectedOption, setSelectedOption, noteData, setNoteData, editNoteTagShow, setEditNoteTagShow, setEditedOption, Loading }) => {
   // ###########NOTE TAGS DISPLAY##########
    const { Option, SingleValue } = components;
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    const emptyNoteTag = (props) => (
            <div className='row'>
                <div className='col-12'></div>
            </div>
    );

    const fullFilledNoteTag = (props) => (
        <div className='row'>
            <div className='col-4'> 
                { props.data.label }
            </div>
            { (props.data.label === 'School') || (props.data.label === 'Work') || (props.data.label === 'Hobby')  ? ''
            : 
                <div className='col-8'>
                    <button type="button" title="Edit note tag" className="close form-button" aria-label="Close" onClick={() => popUpEditTag(props.data.value, props.data.label)}>
                        <FiEdit />
                    </button>
                    <button type="button" title="Delete note tag" className="close form-button" aria-label="Close" onClick={() => deleteTag(props.data.value, props.data.label)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            }
        </div>
    );

    const customSingleValue = (props) => (
        <SingleValue {...props}>
            {selectedOption.label === '' ? emptyNoteTag(props) : fullFilledNoteTag(props)}
        </SingleValue>        
    );

    const CustomSelectOption = props => (
        <Option {...props}>            
            { props.data.label }
            { (props.data.label === 'School') || (props.data.label === 'Work') || (props.data.label === 'Hobby')  ? ''
            : 
                <Fragment> 
                    <button type="button" title="Edit note tag" className="close form-button" aria-label="Close" onClick={() => popUpEditTag(props.data.value, props.data.label)}>
                        <FiEdit />
                    </button>
                    <button type="button" title="Delete note tag" className="close form-button" aria-label="Close" onClick={() => deleteTag(props.data.value, props.data.label)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Fragment> 
            }  
        </Option>
    );

    // ###########NOTE TAGS FUNCTIONS##########

    const tagInputChange = (inputValue: any, actionMeta: any) => {
        console.group('Input Changed');
        console.log(inputValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    };

    const tagChange = (newValue: any, actionMeta: any) => {
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
        setSelectedOption({ value: newValue.value, label: newValue.label });
        if (newValue.__isNew__) {
            const body = JSON.stringify({
                'withCredentials': true,
                user_id: email_global,
                tag_name: newValue.value,
                share: 0 // TODO: share or not?
            });
            axios.post(`${process.env.REACT_APP_API_URL}/profile/note-tag`, body, config)
            .then(res => {
                //TODO: error to dispatch(setAlert('Alert', 'success'));
                console.log(res.data)
                setNoteTags([...NoteTags, 
                    {
                        id: res.data.noteTag.id,
                        tag_name: res.data.noteTag.tag_name,
                        share: res.data.noteTag.share,
                        user_id: email_global
                    }
                ]);
                setNoteData({ ...noteData, note_tag_id: res.data.noteTag.id });
            })
            .catch(err => {
                console.log(err)
            })
        } else {setNoteData({ ...noteData, note_tag_id: newValue.value });}
    };

    const deleteTag = (id, label) => {
        const body = JSON.stringify({
            'withCredentials': true,
            tag_id: id,
        });
        const deleteConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        deleteConfig.data = body;
        axios.delete(`${process.env.REACT_APP_API_URL}/profile/note-tag`, deleteConfig)
            .then(res => {
                console.log(res.data);
                console.log(id, label);
                console.log(selectedOption);
                const note = NoteTags.filter(note => note.id !== id);
                setNoteTags(note);

                if (label === selectedOption.label) {
                    setSelectedOption({ value: '', label: '' });
                } else {
                    setSelectedOption({ value: selectedOption.value, label: selectedOption.label });
                }

            }).catch(err => {
                console.log(err)
            })
    };

    const popUpEditTag = (value, label) => {
        setEditNoteTagShow(!editNoteTagShow);
        setEditedOption({ value: value, label: label });
    };


    return (
        <>
            {Loading && <p>Loading...</p>}
                {!Loading && (
                    <>
                        <CreatableSelect
                            className='w-100'
                            components={{ Option: CustomSelectOption, SingleValue: customSingleValue }}
                            value={selectedOption}
                            onChange={tagChange}
                            onInputChange={tagInputChange}
                            placeholder='Select, search or add...'
                            options = {NoteTags.map((noteTag) => {
                                return { value: noteTag.id, label: noteTag.tag_name};
                            })} 
                        />
                    </>
                )}
        </>
    )
}

export default NoteTag;