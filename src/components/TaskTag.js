import { components } from 'react-select';
import { FiEdit } from "react-icons/fi";
import React, { Fragment } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';
import CreatableSelect from 'react-select/creatable';

const TaskTag = ({ email_global, taskTags, setTaskTags, selectedOption, setSelectedOption, formData, setFormData, editTaskTagShow, setEditTaskTagShow, setEditedOption, Loading }) => {
   // ###########TASK TAGS DISPLAY##########
    const { Option, SingleValue } = components;
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    const emptyTaskTag = (props) => (
            <div className='row'>
                <div className='col-12'></div>
            </div>
    );
 
    const fullFilledTaskTag = (props) => (
        <div className='row'>
            <div className='col-4'> 
                { props.data.label }
            </div>
            { (props.data.label === 'School') || (props.data.label === 'Work') || (props.data.label === 'Hobby')  ? ''
            : 
                <div className='col-8'>
                    <button title="Edit task tag" type="button" className="close form-button" aria-label="Close" onClick={() => popUpEditTag(props.data.value, props.data.label)}>
                        <FiEdit />
                    </button>
                    <button title="Delete task tag" type="button" className="close form-button" aria-label="Close" onClick={() => deleteTag(props.data.value, props.data.label)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            }
        </div>
    );

    const customSingleValue = (props) => (
        <SingleValue {...props}> 
            {selectedOption.label === '' ? emptyTaskTag(props) : fullFilledTaskTag(props)}
        </SingleValue>        
    );

    const CustomSelectOption = props => (
        <Option {...props}>            
            { props.data.label }
            { (props.data.label === 'School') || (props.data.label === 'Work') || (props.data.label === 'Hobby')  ? ''
            : 
                <Fragment>
                    <button type="button" title="Edit task tag" className="close form-button" aria-label="Close" onClick={() => popUpEditTag(props.data.value, props.data.label)}>
                        <FiEdit />
                    </button>
                    <button type="button" title="Delete task tag" className="close form-button" aria-label="Close" onClick={() => deleteTag(props.data.value, props.data.label)}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Fragment> 
            }  
        </Option>
    );

    // ###########TASK TAGS FUNCTIONS##########

    const tagInputChange = (inputValue: any, actionMeta: any) => {
        console.log(inputValue);
    };

    const tagChange = (newValue: any, actionMeta: any) => {
        setSelectedOption({ value: newValue.value, label: newValue.label });
        if (newValue.__isNew__) {
            const body = JSON.stringify({
                'withCredentials': true,
                user_id: email_global,
                tag_name: newValue.value,
                share: 0 // TODO: share or not?
            });
            axios.post(`${process.env.REACT_APP_API_URL}/profile/task-tag`, body, config)
            .then(res => {
                //TODO: error to dispatch(setAlert('Alert', 'success'));
                setTaskTags([...taskTags, 
                    {
                        id: res.data.taskTag.id,
                        tag_name: res.data.taskTag.tag_name,
                        share: res.data.taskTag.share,
                        user_id: email_global
                    }
                ]);
            })
            .catch(err => {
                console.log(err)
            })
        };
        setFormData({ ...formData, task_tag: newValue.label });
    };

    const deleteTag = (id, label) => {
        const body = JSON.stringify({
            'withCredentials': true,
            tag_id: id,
        });
        const deleteConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        deleteConfig.data = body;
        axios.delete(`${process.env.REACT_APP_API_URL}/profile/task-tag`, deleteConfig)
            .then(res => {
                const Tasks = taskTags.filter(task => task.id !== id);
                setTaskTags(Tasks);

                if (label === selectedOption.label) {
                    setSelectedOption({ value: '', label: '' });
                } else {
                    setSelectedOption({ value: selectedOption.value, label: selectedOption.label });
                }

            }).catch(err => {
                console.log(err)
            })
    };

    const popUpEditTag = (value, label) => {
        setEditTaskTagShow(!editTaskTagShow);
        setEditedOption({ value: value, label: label });
    };


    return (
        <>
            {Loading && <p>Loading...</p>}
                {!Loading && (
                    <>
                        <CreatableSelect
                            className='w-100'
                            components={{ Option: CustomSelectOption, SingleValue: customSingleValue }}
                            value={selectedOption}
                            onChange={tagChange}
                            onInputChange={tagInputChange}
                            placeholder='Select, search or add...'
                            options = {taskTags.map((taskTag) => {
                                return { value: taskTag.id, label: taskTag.tag_name};
                            })} 
                        />
                    </>
                )}
        </>
    )
}

export default TaskTag;