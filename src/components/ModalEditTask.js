import React, { useState, useEffect, useRef } from 'react';
import { Modal, Button, InputGroup, FormControl, Form } from 'react-bootstrap';
import Cookies from 'js-cookie';
import axios from 'axios';
import ModalTaskTagEdit from './ModalEditTaskTag';
import TaskTag from './TaskTag';
import moment from 'moment';

const ModalEditTask = ({ editTaskShow, setEditTaskShow, editedTaskOption, setEditedTaskOption, setTasks, Tasks, taskTags, setTaskTags, Loading, setLoading, email_global }) => {
    const popUpEditTask = (value, label) => {
        setEditTaskShow(!editTaskShow);
        const nullTaskData = {
            title: '',
            tag_name: '',
            date: '',
            periodicity: '',
            importance: ''
        };
        setEditTaskData(nullTaskData);
        setEditedTaskOption('');
    };

    const [selectedTagOption, setSelectedTagOption] = useState();
    const [editTaskTagShow, setEditTaskTagShow] = useState(false);
    const [editedTagOption, setEditedTagOption] = useState({ label: '', value: '' });
    const [everydayCheckbox, setEverydayCheckbox] = useState(false);
    const [editTaskData, setEditTaskData] = useState({
        id: '',
        title: '',
        tag_name: '',
        date: '',
        periodicity: '',
        importance: ''
    });
    if((editedTaskOption != '' || editedTaskOption === 0) != '' && editTaskData.title == '' && Tasks != []) 
    {   
        let perio = Tasks[editedTaskOption].periodicity
        
        if(typeof(Tasks[editedTaskOption].periodicity) != 'object')
            perio = JSON.parse(perio.replace(/'/g,'"'));

        const actualTaskData = {
            id: Tasks[editedTaskOption].id,
            title: Tasks[editedTaskOption].title,
            task_tag: Tasks[editedTaskOption].task_tag,
            date: Tasks[editedTaskOption].date,
            periodicity: perio,
            importance: Tasks[editedTaskOption].importance
        };
        if(Tasks[editedTaskOption].task_tag != null){
            let index = taskTags.findIndex((taskTag) => taskTag.title === Tasks[editedTaskOption].task_tag);
            setSelectedTagOption({ value: index, label: Tasks[editedTaskOption].task_tag });
        }
        setEditTaskData(actualTaskData);
    }
    const editTaskToBackend = () => {
        let not;
            if (Tasks[editedTaskOption].notification)
                not = 1;
            else
                not = 0;
            let don;
            if (Tasks[editedTaskOption].done)
                don = 1;
            else
                don = 0;

        const editTaskbody = JSON.stringify({
            'withCredentials': true,
            id: editTaskData.id,
            title: editTaskData.title,
            importance: editTaskData.importance,
            notification: Tasks[editedTaskOption].notification,
            done: Tasks[editedTaskOption].done,
            task_tag: editTaskData.task_tag,
            date: editTaskData.date,
            periodicity: JSON.stringify(editTaskData.periodicity),
        });
        const editTaskConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        axios.put(`${process.env.REACT_APP_API_URL}/profile/task`, editTaskbody, editTaskConfig)
            .then(res => {
                console.log(res.data);
                let newTasks = [...Tasks];
                newTasks[editedTaskOption] = {
                    id: editTaskData.id,
                    title: editTaskData.title,
                    importance: editTaskData.importance,
                    notification: not,
                    done: don,
                    task_tag: editTaskData.task_tag,
                    date: editTaskData.date,
                    periodicity: editTaskData.periodicity,
                };
                setTasks(newTasks);
                popUpEditTask();
            }).catch(err => {
                console.log(err)
                popUpEditTask();
            })
    };
    useEffect(() => {
        console.log(editTaskData);
    }, [editTaskData]);

    const onChangePerCheckbox = e => {
        if (e.target.value == 1)
            e.target.value = 0;
        else 
            e.target.value = 1;

        let days = editTaskData.periodicity
        days[e.target.name] = parseInt(e.target.value);
        setEditTaskData({ ...editTaskData, periodicity: days })
    }
    
    const isInitialMount = useRef(true);
    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
         } else {
            let days = editTaskData.periodicity
            if(everydayCheckbox) {
                Object.entries(days).map((entry, i) => {
                    if(i != 6 && days[entry[0]] != 1)
                        days[entry[0]] = 1;
                })
            }
            else {
                Object.entries(days).map((entry, i) => {
                    if(i != 6 && days[entry[0]] != 0)
                        days[entry[0]] = 0;
                })
            }
            setEditTaskData({ ...editTaskData, periodicity: days })
        }
    }, [everydayCheckbox])

    return (
        <Modal show={editTaskShow} onHide={popUpEditTask}>

            <Modal.Header closeButton>
                <Modal.Title>Edit Task</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Title</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        name="title"
                        aria-label="Default"
                        aria-describedby="inputGroup-sizing-default"
                        defaultValue={editTaskData.title}
                        onChange={e => setEditTaskData({...editTaskData, [e.target.name]: e.target.value})}
                        required    
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Task tag</InputGroup.Text>
                    </InputGroup.Prepend>
                    <div className='edit_tag_in_task'>
                        <TaskTag
                            email_global={email_global}
                            taskTags={taskTags}
                            setTaskTags={setTaskTags}
                            selectedOption={selectedTagOption}
                            setSelectedOption={setSelectedTagOption}
                            formData={editTaskData}
                            setFormData={setEditTaskData}
                            editTaskTagShow={editTaskTagShow}
                            setEditTaskTagShow={setEditTaskTagShow}
                            setEditedOption={setEditedTagOption}
                            Loading={Loading} 
                        />
                    </div>
                    <ModalTaskTagEdit 
                        editedOption={editedTagOption}
                        setEditedOption={setEditedTagOption}
                        editTaskTagShow={editTaskTagShow} 
                        setEditTaskTagShow={setEditTaskTagShow} 
                        selectedOption={selectedTagOption} 
                        setSelectedOption={setSelectedTagOption} 
                        setTaskTags={setTaskTags} 
                        taskTags={taskTags}
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Date</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        name="date"
                        type="date"
                        aria-label="Default"
                        aria-describedby="inputGroup-sizing-default"
                        defaultValue={editTaskData.date ? moment(new Date(editTaskData.date)).format('YYYY-MM-DD') : null }
                        onChange={e => setEditTaskData({...editTaskData, [e.target.name]: e.target.value})}
                    />
                </InputGroup>
                <Form.Label>Periodicity</Form.Label>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Monday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox
                        name="monday" 
                        aria-label="Checkbox for Monday"
                        checked={editTaskData.periodicity['monday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)}
                    />
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Tuesday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox
                        name="tuesday"
                        aria-label="Checkbox for Tuesday"
                        checked={editTaskData.periodicity['tuesday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)}
                    />
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Wednesday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox
                        name="wednesday"
                        aria-label="Checkbox for Wednesday" 
                        checked={editTaskData.periodicity['wednesday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)}
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Thursday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox 
                        name="thursday"
                        aria-label="Checkbox for Thursday" 
                        checked={editTaskData.periodicity['thursday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)}    
                    />
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Friday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox 
                        name="friday"
                        aria-label="Checkbox for Friday" 
                        checked={editTaskData.periodicity['friday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)}
                    />
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Saturday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox 
                        name="saturday"
                        aria-label="Checkbox for Saturday" 
                        checked={editTaskData.periodicity['saturday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)}
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Sunday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox
                        name="sunday" 
                        aria-label="Checkbox for Sunday"
                        checked={editTaskData.periodicity['sunday'] == 1 ? 'checked' : ''}
                        onChange={e => onChangePerCheckbox(e)} 
                    />
                    <InputGroup.Prepend>
                        <InputGroup.Text id="inputGroup-sizing-default">Everyday</InputGroup.Text>
                    </InputGroup.Prepend>
                    <InputGroup.Checkbox 
                        name="everyday"
                        aria-label="Checkbox for Everyday" 
                        checked={everydayCheckbox ? 'checked' : ''}
                        onChange={e => setEverydayCheckbox(!everydayCheckbox)} 
                        />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Importance</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        as="select"
                        name="importance"
                        aria-label="Default"
                        aria-describedby="inputGroup-sizing-default"
                        defaultValue={editTaskData.importance}
                        onChange={e => setEditTaskData({...editTaskData, [e.target.name]: e.target.value})}
                    >
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </FormControl>
                </InputGroup>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={popUpEditTask}>Close</Button>
                <Button variant="primary" onClick={() => editTaskToBackend()}>Ok</Button>
            </Modal.Footer>

        </Modal>
    );
}

export default ModalEditTask;