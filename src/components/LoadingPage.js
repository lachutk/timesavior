import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';

const LoadingPage = ({ isAuthenticated }) => {

    const guestText = () => (
        <Fragment>
            <p>Na sam początek powinieneś założyć konto lub się zalogować(w zakładce "Login" jest przygotowane gotowe konto) poprzez poniższe przyciski "Login" i "Sign Up".</p>
            <hr />
            <p className='lead'>
                <Link className='btn btn-primary btn-lg mr-2' to='/login' role='button'>Login</Link>
                <Link className='btn btn-primary btn-lg mr-2' to='/signup' role='button'>Sign Up</Link>
            </p>
            <p>W nich jest możliwość rejestracji.</p> 
            <p>Po rejestracji na podany mail powinien przyjść mail z linkiem do aktywacji, trzeba w niego wejść wcisnąć "verify".</p>
            <p>Po tej akcji można już normalnie się zalogować i zobaczyć możliwości aplikacji - Tam znajdziesz dalszą część instrukcji :)</p>
            <br />
        </Fragment>
    );

    const authUserText = () => (
        <Fragment>
            <p className='lead'><b>W zakładkach znajdziesz krótkie opisy.</b> Tutaj rozpisałem całość bardziej szczegółowo.</p>
            <p>Na sam początek wyświetla nam się ten wpis, jest on pod przyciskiem nawigacyjnym "Home".</p>
            <br />
            <p className='lead'>Zakładka Tasks</p>
            <p>Ogólnie rzecz ujmując, tutaj możemy dodawać, edytować, usuwać, przeglądać zadania, oraz dodawać do nich notatki. Do każdego takiego zadania możemy dodać:</p>
                <ul> 
                    <li>Tytuł</li>
                    <li>Task tag - <i>Kategorie, który będziemy mogli przypisywać do zadań. Można dodawać nowe, edytować, oraz usuwać.</i></li>
                    <li>Datę, na kiedy chcemy sobie to zadanie przypisać. </li>
                    <li>Powtarzalność, aby dane zadanie powtarzało się w konkretne dni tygodnia.</li>
                    <li>Ważność<i> skala 1-5</i>, aby nadać ważność (1) najważniejszym, oraz (5) mniej ważnym, na podstawie czego zadania kolorowane są w tabeli.</li>
            </ul>
            <p>Po dodaniu zadania pojawiają się w tabeli, gdzie można je dowolnie wyszukiwać, oraz po wcisnięciu w tytuł komórek sortować.</p>
            <p>Przy każdym zadaniu widnieją 4 przyciski, <i>które po najechaniu pokazują do czego służy dany przycisk</i>:</p>
            <ul>
                <li>Done - ten przycisk zaznaczamy jak wykonaliśmy zadanie.</li>
                <li>Edit task - po naciśnięciu tego przycisku, wyskoczy okienko z możliwością edycji zadań.</li>
                <li>Add note - gdy wciśniemy, wyskoczy okienko z możliwością dodania notatki do konktrengeo zadania. Można też do nich dodać kategorie notatek, po których można je wyszukiwać i edytować w zakładce "Notes".</li>
                <li>Delete task - permamentnie usuwa zadanie.</li>
            </ul>
            <p>Pod tabelą są przyciski do przemieszczania się pomiędzy kolejnymi stronami zadań, oraz do zmiany wyświetlania ilości zadań na jednej stronie.</p>
            <p>Po wciśnięciu przycisku "Show Icalendar menu" kryje się menu, gdzie można wgrać plik o formacie ".ics", z którego zostaną pobrane zadania.</p>
            <br />
            <p className='lead'>Zakładka Notes</p>
            <p> Tutaj możemy dodawać, edytować i usuwać notatki z odpowiednimi kategoriami/tagami notatek(które można dodawać, edytować i usuwać), oraz przypisać je do konkretnych zadań.</p>
            <p>Jest też możliwość dodawania, edytowania i usuwania kategori/tagów notatek.</p>
            <br />
            <p className='lead'>Zakładka Friends</p>
            <p>Po lewej stronie możemy dodać jakiegoś użytkownika po adresie email, na środku mamy zadania udostępnione przez innych użytkowników. </p>
            <p>Po prawej mamy tagi zadań, którymi po wciśnięciu przycisku "share" możemy się podzielić z użykownikami dodanymi do "friends".</p>
            <br />
            <p className='lead'>Zakładka Profile</p>
            <p>Tutaj możemy zmienić imię i nazwisko, sprawdzić adres email, oraz usunąć konto.</p>
            <br />
            <p className='lead'>Zakładka Pomodoro</p>
            <p>Jest to stoper, na którym możemy ustawić czas i kliknąć "start". Po danym czasie odpala się alarm. Przydatne do nauki, pracy czy innych czynności wymagających przerw.</p>
            <p>Strona stworzona jest w języku angielskim. </p>
        </Fragment>
    );
    return (
        <div className='container'>
            <Helmet>
                <title>Your Time Savior</title>
                <meta name='Home' content='Home page' />
            </Helmet>
            <div className='jumbotron mt-6'>
                <h1 className='display-4'>Witam w pracy inżynierskiej YourTimeSavior</h1>
                <p className='lead'>Jest to system do organizacji czasu pracy, do realizacji projektów, czy innych celów.</p>
                <p className='lead'>Po przetestowaniu proszę o uzupełnienie ankiety.{' '}
                    <a className='btn btn-primary btn-lg mr-2' href='https://forms.gle/idc8ZmNNVvP4q7KY8' role='button' target="_blank">Ankieta</a>
                    Dzięki! :)
                </p>
                {isAuthenticated ? authUserText() : guestText()}
            </div>
        </div>

    );
}

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});
export default connect(mapStateToProps)(LoadingPage);