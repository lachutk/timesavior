import React, { useMemo, useState, useEffect } from 'react';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import { BsFillCaretUpFill, BsFillCaretDownFill } from "react-icons/bs";
import { ColumnFilter } from './ColumnFilter';
import { format } from 'date-fns';
import axios from 'axios';
import Cookies from 'js-cookie';
import { FiEdit } from "react-icons/fi";
import ReactReadMoreReadLess from "react-read-more-read-less";
import NoteEditForm from './NoteEditForm';
import { GrClose } from 'react-icons/gr';
 
const NoteTable = ({ Tasks, notes, setNotes, noteTags, setNoteTags, loadingNotes, loadingNoteTags }) => {
    const [showEditNoteForm, setShowEditNoteForm] = useState(false);
    const [editedNoteOption, setEditedNoteOption] = useState();
    const columns = useMemo(() => {
        const columns = [
            {
                Header: 'Note',
                accessor: 'note',
            },
            {
                Header: 'Note tag',
                accessor: 'note_tag',
                Cell: ({ value }) => { 
                    let index = noteTags.findIndex((noteTag) => noteTag.id === value);
                    if(noteTags[index])
                        return noteTags[index].tag_name
                    else return ''
                }
            },
            {
                Header: 'Task',
                accessor: 'task_id',
                Cell: ({ value }) => { 
                    let index = Tasks.findIndex((task) => task.id === value);
                    if(Tasks[index])
                        return Tasks[index].title
                    else return ''
                }
            },
            {
                Header: 'Date',
                accessor: 'date',
                Cell: ({ value }) => { return value ? format(new Date(value), 'd.MM.yyyy') : '' },
            }
        ];
        return columns;
    }, [notes])
    const data = useMemo(() => notes, [notes])

    const defaultColumn = useMemo(() => {
        return {
            Filter: ColumnFilter
        }
    }, [])

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        nextPage,
        previousPage,
        canNextPage,
        canPreviousPage,
        pageOptions,
        gotoPage,
        pageCount,
        setPageSize,
        prepareRow,
        state,
    } = useTable({
        columns,
        data,
        defaultColumn,
        initialState: { pageSize: 10 }
    },
        useSortBy,
        useFilters,
        usePagination
    );

    const deleteNote = (id) => {
        const body = JSON.stringify({
            'withCredentials': true,
            note_id: id,
        });
        const deleteConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        deleteConfig.data = body;
        console.log(body);
        axios.delete(`${process.env.REACT_APP_API_URL}/profile/note`, deleteConfig)
            .then(res => {
                console.log(res.data);
                const newNotes = notes.filter(note => note.id !== id);
                setNotes(newNotes);
            }).catch(err => {
                console.log(err)
            })
    };

    const popUpEditNote = (id) => {
        let thisNoteIndex = notes.findIndex((note) => note.id === id)
        let thisTaskIndex = Tasks.findIndex((task) => task.id === notes[thisNoteIndex].task_id)
        let thisNoteTagIndex = noteTags.findIndex((noteTag) => noteTag.id === notes[thisNoteIndex].note_tag)
        let thisTask = ''
        let thisNoteTag = ''
        let thisNote = ''
        if(thisTaskIndex != -1)
            thisTask = Tasks[thisTaskIndex]
        if(thisNoteTagIndex != -1)
            thisNoteTag = noteTags[thisNoteTagIndex]
        if(thisNoteIndex != -1)
            thisNote = notes[thisNoteIndex]
        setEditedNoteOption({ id: thisNoteIndex, task: thisTask, tag: thisNoteTag, note: thisNote });
    };
    useEffect(() => {
        if(editedNoteOption)
            setShowEditNoteForm(!showEditNoteForm);
    }, [editedNoteOption])

    const { pageIndex, pageSize } = state
    return (
        <>  
            {showEditNoteForm ?
            <>
                <button type="button" className="close show-button" aria-label="Close" onClick={() => setShowEditNoteForm(!showEditNoteForm)}>
                    <GrClose size={48}/>
                </button>
                <NoteEditForm 
                    thisNote={editedNoteOption}
                    Tasks={Tasks}
                    notes={notes}
                    setNotes={setNotes}
                    noteTags={noteTags}
                    setNoteTags={setNoteTags}
                    loadingNoteTags={loadingNoteTags}
                    showEditNoteForm={showEditNoteForm}
                    setShowEditNoteForm={setShowEditNoteForm}
                />
            </>
            : '' }
            <table className='note_table mt-3' {...getTableProps()}>
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => {
                                return(
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        {column.render('Header')}
                                        <span>{column.canFilter ? column.render('Filter') : null}</span>
                                        <span>
                                            {column.isSorted ? (column.isSortedDesc ? <BsFillCaretDownFill /> : <BsFillCaretUpFill />) : ''}
                                        </span>
                                    </th>
                                )
                            })
                            }
                            <th colspan="5">Operations</th>
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row);
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map((cell) => {
                                    return (
                                        <td {...cell.getCellProps()}>
                                            { cell.column.Header == "Note" ?
                                            <ReactReadMoreReadLess
                                               charLimit={200}
                                               readMoreText={"Read more ▼"}
                                               readLessText={"Read less ▲"}
                                            >
                                                {cell.row.original.note}
                                            </ReactReadMoreReadLess>
                                            : <>{cell.render('Cell')}</>}
                                        </td>
                                    )
                                })}
                                <>
                                    <td>
                                        <button type="button" title="Edit note" className="close form-button" onClick={() => popUpEditNote(row.original.id)}>
                                            <FiEdit />
                                        </button>
                                    </td>
                                    <td>
                                        <button type="button" title="Delete note" className="close form-button" onClick={() => deleteNote(row.original.id)}>
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </td>
                                </>                                    
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <form className='form-inline justify-content-center mt-3'>
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>
                <div className="form-group ml-1">
                    <label class="control-label">| Go to page: {' '}</label>
                    <input type='number' className='form-control ml-1' defaultValue={pageIndex + 1}
                        onChange={e => {
                            const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                            gotoPage(pageNumber)
                        }}
                        style={{ width: '50px' }}
                    />{' '}
                </div>
                <div className="form-group">
                    <select className='form-control ml-1' value={pageSize} onChange={e => setPageSize(Number(e.target.value))}>
                        {
                            [10, 25, 50].map(pageSize => (
                                <option key={pageSize} value={pageSize}>
                                    Show {pageSize}
                                </option>
                            ))
                        }
                    </select>
                </div>
            </form>
            <div className='mt-2 text-center'>
                <button type="button" title="First" class="btn btn-default" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                <button type="button" title="Previous" class="btn btn-default" onClick={() => previousPage()} disabled={!canPreviousPage}>Previous</button>
                <button type="button" title="Next" class="btn btn-default" onClick={() => nextPage()} disabled={!canNextPage}>Next</button>
                <button type="button" title="Last" class="btn btn-default" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>{'>>'}</button>
            </div>
        </>
    );
}

export default NoteTable;