import React, { useMemo, useState } from 'react';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import { ColumnFilter } from './ColumnFilter';
import axios from 'axios';
import Cookies from 'js-cookie';
import { BsFillCaretUpFill, BsFillCaretDownFill } from "react-icons/bs";
import { RiShareForwardFill, RiShareForwardLine } from 'react-icons/ri';
 
const TaskTagsShareTable = ({ taskTags, setTaskTags }) => {
    const columns = useMemo(() => {
        const columns = [
            {
                Header: 'Tage Name',
                accessor: 'tag_name',
            }
        ];
        return columns;
    }, [taskTags])
    const data = useMemo(() => taskTags, [taskTags])
    const defaultColumn = useMemo(() => {
        return {
            Filter: ColumnFilter
        }
    }, [])

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        nextPage,
        previousPage,
        canNextPage,
        canPreviousPage,
        pageOptions,
        gotoPage,
        pageCount,
        setPageSize,
        prepareRow,
        state,
    } = useTable({
        columns,
        data,
        defaultColumn,
        initialState: { pageSize: 10 }
    },
        useSortBy,
        useFilters,
        usePagination
    );

    const share = (id, tag_name) => {
        let index = taskTags.findIndex((tag) => tag.id === id)
        const body = JSON.stringify({
            'withCredentials': true,
            tag_id: id,
            tag_name: tag_name,
            share: !taskTags[index].share
        });
        const deleteConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        console.log(body);
        axios.put(`${process.env.REACT_APP_API_URL}/profile/task-tag`, body, deleteConfig)
            .then(res => {
                console.log(res.data);
                let newTaskTags = [...taskTags];
                newTaskTags[index] = { ...taskTags[index], share: !taskTags[index].share }
                setTaskTags(newTaskTags);
            }).catch(err => {
                console.log(err)
            })
    };
    const { pageIndex, pageSize } = state
    return (
        <>
            <table className='note_table mt-3' {...getTableProps()}>
                <thead>
                    {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map((column) => {
                                return(
                                    <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                        {column.render('Header')}
                                        <span>{column.canFilter ? column.render('Filter') : null}</span>
                                        <span>
                                            {column.isSorted ? (column.isSortedDesc ? <BsFillCaretDownFill /> : <BsFillCaretUpFill />) : ''}
                                        </span>
                                    </th>
                                )
                            })
                            }
                            <th>Share</th>
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row);
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map((cell) => {
                                    return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                })}
                                <td className='center'>
                                    <button type="Share" title="Share" className="close form-button" onClick={() => share(row.original.id, row.original.tag_name)}>
                                        {row.original.share ? <RiShareForwardFill /> : <RiShareForwardLine />}
                                    </button>
                                </td>                                  
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <form className='form-inline justify-content-center mt-3'>
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>
                <div className="form-group ml-1">
                    <label class="control-label">| Go to page: {' '}</label>
                    <input type='number' className='form-control ml-1' defaultValue={pageIndex + 1}
                        onChange={e => {
                            const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                            gotoPage(pageNumber)
                        }}
                        style={{ width: '50px' }}
                    />{' '}
                </div>
                <div className="form-group">
                    <select className='form-control ml-1' value={pageSize} onChange={e => setPageSize(Number(e.target.value))}>
                        {
                            [10, 25, 50].map(pageSize => (
                                <option key={pageSize} value={pageSize}>
                                    Show {pageSize}
                                </option>
                            ))
                        }
                    </select>
                </div>
            </form>
            <div className='mt-2 text-center'>
                <button type="button" title="First" class="btn btn-default" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                <button type="button" title="Previous" class="btn btn-default" onClick={() => previousPage()} disabled={!canPreviousPage}>Previous</button>
                <button type="button" title="Next" class="btn btn-default" onClick={() => nextPage()} disabled={!canNextPage}>Next</button>
                <button type="button" title="Last" class="btn btn-default" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>{'>>'}</button>
            </div>
        </>
    )
}

export default TaskTagsShareTable;