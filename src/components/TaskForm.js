import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import { connect } from 'react-redux';
import ModalTaskTagEdit from './ModalEditTaskTag';
import TaskTag from './TaskTag';
import { setAlert } from '../actions/alert';
 
const TaskForm = ({ email_global, setAlert, Tasks, setTasks, taskTags, setTaskTags, Loading, setLoading, setShowForm, showForm }) => {
    const [per, setPer] = useState({
        monday: 0,
        tuesday: 0,
        wednesday: 0,
        thursday: 0,
        friday: 0,
        saturday: 0,
        sunday: 0
    });
    const [formData, setFormData] = useState({
        title: '',
        task_tag: null,
        date: null,
        periodicity: [{
            monday: 0,
            tuesday: 0,
            wednesday: 0,
            thursday: 0,
            friday: 0,
            saturday: 0,
            sunday: 0,
        }],
        importance: 3,
        notification: false,
        done: false,
        monday: false,
        tuesday: false,
        wednesday: false,
        thursday: false,
        friday: false,
        saturday: false,
        sunday: false,
    });
    const [editTaskTagShow, setEditTaskTagShow] = useState(false);
    const [selectedOption, setSelectedOption] = useState();
    const [editedOption, setEditedOption] = useState({ label: '', value: '' });
    const [everydayCheckbox, setEverydayCheckbox] = useState(false);
    const { title, task_tag, date, periodicity, importance, notification, done, monday, tuesday, wednesday, thursday, friday, saturday, sunday } = formData;
    
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    // ###########TASK##########
    //after setper set FormData with values from per
    useEffect(() => {
        setFormData({ ...formData, ['periodicity']: per });
    }, [per]);

    useEffect(() => {
        console.log(Tasks);
    }, [Tasks])

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });
    const onChangePeriodicity = async (e) => 
    {
        if (e.target.value == 1)
            e.target.value = 0;
        else 
            e.target.value = 1;

        let day = e.target.name
        let value = e.target.value
        setFormData({ ...formData, [day]: parseInt(value) });
        setPer({...per, [day]: parseInt(value) });
    };

    const isInitialMount = useRef(true);
    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
         } else {
            console.log(everydayCheckbox);
            let days = formData.periodicity
            if(everydayCheckbox) {
                Object.entries(days).map((entry, i) => {
                    if(i != 7)
                        days[entry[0]] = 1;
                        setFormData({ ...formData, [entry[0]]: 1 });
                })
            }
            else {
                Object.entries(days).map((entry, i) => {
                    if(i != 7)
                        days[entry[0]] = 0;
                        setFormData({ ...formData, [entry[0]]: 0 });
                })
            }
            console.log(days);
            setPer(days)
            console.log(per);
        }
    }, [everydayCheckbox])

    const onChangeCheckbox = e => 
    {
        if (e.target.value === true)
            e.target.value = false;
        else 
            e.target.value = true;

        setFormData({ ...formData, [e.target.name]: e.target.value });
    }

    const setEmptyForm = () => {
        setFormData({
            title: '',
            task_tag: null,
            date: null,
            periodicity: [{
                monday: 0,
                tuesday: 0,
                wednesday: 0,
                thursday: 0,
                friday: 0,
                saturday: 0,
                sunday: 0,
            }],
            importance: 3,
            notification: false,
            done: false,
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false,
        });
    }

    const onSubmit = e => {
        e.preventDefault();
        console.log(formData);
        axios.defaults.headers = {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        };
        // let not;
        // if (notification)
        //     not = 1;
        // else
        //     not = 0;
        // let don;
        // if (done)
        //     don = 1;
        // else
        //     don = 0;

        const body = JSON.stringify({
            'withCredentials': true,
            user_id: email_global,
            title: title,
            importance: importance,
            notification: notification,
            done: done,
            task_tag: task_tag,
            date: date,
            periodicity: JSON.stringify(periodicity),
        });
        axios.post(`${process.env.REACT_APP_API_URL}/profile/task`, body)
        .then(res => {
            console.log(res.data);

            setTasks([ {
                id: res.data.id,
                title: title,
                date: date,
                periodicity: JSON.stringify(periodicity),
                task_tag: task_tag,
                importance: importance,
                notification: notification,
                done: done,
                user_id: res.data.user_id
            }, ...Tasks, ]);
            setShowForm(!showForm);
            setAlert('Task added successfully', 'success');
            setEmptyForm();
        })
        .catch(err => {
            console.log(err)
            setShowForm(!showForm);
            setEmptyForm();
            setAlert('Task added failed', 'error');
        })
    };

    return (
        <div className='container'>
            <ModalTaskTagEdit 
                editedOption={editedOption}
                setEditedOption={setEditedOption}
                editTaskTagShow={editTaskTagShow} 
                setEditTaskTagShow={setEditTaskTagShow} 
                selectedOption={selectedOption} 
                setSelectedOption={setSelectedOption} 
                setTaskTags={setTaskTags} 
                taskTags={taskTags}
            />
            <form onSubmit={e => onSubmit(e)}>
                <div className='form-group'>
                    <label className='form-label' htmlFor='title'>Title</label>
                    <input className='form-control' type='text' placeholder='Title' name='title' value={title} onChange={e => onChange(e)} required />
                </div>                
                <div className='form-group'>
                    <label className='form-label' htmlFor='task_tag'>Task tag</label>

                    <TaskTag 
                        email_global={email_global}
                        taskTags={taskTags}
                        setTaskTags={setTaskTags}
                        selectedOption={selectedOption}
                        setSelectedOption={setSelectedOption}
                        formData={formData}
                        setFormData={setFormData}
                        editTaskTagShow={editTaskTagShow}
                        setEditTaskTagShow={setEditTaskTagShow}
                        setEditedOption={setEditedOption}
                        Loading={Loading} 
                    />

                </div>
                <div className='form-group'>
                    <label className='form-label' htmlFor='date'>Date</label>
                    <input className='form-control' type='date' name='date' value={date} onChange={e => onChange(e)} />
                </div>
                <div className='form-check'>                
                    <p className='lead' htmlFor='periodicity'>Periodicity:</p>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox'
                    name='monday' value={monday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.monday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='monday'>Monday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox' 
                    name='tuesday' value={tuesday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.tuesday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='tuesday'>Tuesday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox' 
                    name='wednesday' value={wednesday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.wednesday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='wednesday'>Wednesday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox' 
                    name='thursday' value={thursday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.thursday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='thursday'>Thursday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox'
                    name='friday' value={friday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.friday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='friday'>Friday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox'
                    name='saturday' value={saturday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.saturday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='saturday'>Saturday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox' 
                    name='sunday' value={sunday} onChange={e => onChangePeriodicity(e)} 
                    checked={per.sunday == 1 ? 'checked' : ''}/>
                    <label className='form-check-label' htmlFor='sunday'>Sunday</label>
                </div>
                <div className='form-check'> 
                    <input className='form-check-input' type='checkbox' name='everyday' value={everydayCheckbox ? 1 : 0} onChange={e => setEverydayCheckbox(!everydayCheckbox)} />
                    <label className='form-check-label' htmlFor='everyday'>Everyday</label>
                </div>
                <div className='form-group'>
                    <label className='form-label' htmlFor='importance'>Importance</label>
                    <select className='form-control' name='importance' value={importance} onChange={e => onChange(e)}>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                {/* <div className='form-check'>   
                    <input className='form-check-input' type='checkbox' name='notification' value={notification} onChange={e => onChangeCheckbox(e)} />
                    <label className='form-check-label' htmlFor='notification'>Notification</label>
                </div> */}
                {/* <div className='form-check'>   
                    <input className='form-check-input' type='checkbox' name='done' value={done} onChange={e => onChangeCheckbox(e)} />
                    <label className='form-check-label' htmlFor='done'>Done</label>
                </div> */}

                <div className='form-group'>
                    <button className='btn btn-primary' type='submit'>Add task</button>
                </div>
            </form>
        </div>
    )
};

TaskForm.propTypes = {
    setTasksView: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
    email_global: state.profile.email,

});
export default connect(mapStateToProps, { setAlert })(TaskForm);