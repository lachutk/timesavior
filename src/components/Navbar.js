import React, { Fragment, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { logout } from '../actions/auth';
import { connect } from 'react-redux';
import { Navbar, Nav } from 'react-bootstrap';
import Alert from './Alert';

const NavbarMenu = ({ logout, isAuthenticated }) => {
    const [redirect, setRedirect] = useState(false);

    const logout_user = () => {
        logout();
        setRedirect(true);
    };
    const guestLinks = () => (
        <Fragment>
            <Nav.Link as={Link} to='/login'>Login</Nav.Link>
            <Nav.Link as={Link} to='/signup'>Sign up</Nav.Link>
        </Fragment>
    );

    const authUserLinks = () => (
        <Fragment>
            <Nav.Link as={Link} to='/tasks'>Tasks</Nav.Link>
            <Nav.Link as={Link} to='/notes'>Notes</Nav.Link>
            <Nav.Link as={Link} to='/friends'>Friends</Nav.Link>
            <Nav.Link as={Link} to='/profile'>Profile</Nav.Link>
            <Nav.Link as={Link} to='/pomodoro'>Pomodoro</Nav.Link>
            <Nav.Link href='#!' onClick={logout_user}>Logout</Nav.Link>
        </Fragment>
    );

    return (
        <Fragment>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand as={Link} to='/'>Your Time Savior</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to='/'>Home</Nav.Link>
                        {isAuthenticated ? authUserLinks() : guestLinks()}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            {redirect ? <Redirect to='/' /> : <Fragment></Fragment>}
            <Alert />
        </Fragment>
    );

};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});
export default connect(mapStateToProps, { logout })(NavbarMenu);