import React, { useState } from 'react'
import { AiOutlineSearch } from 'react-icons/ai';

export const ColumnFilter = ({ column }) => {
    const { filterValue, setFilter } = column
    const [showSearch, setShowSearch] = useState(false)
    return (
        <div className='text-center'>
            <button type='button' title="Search" className='btn btn-default' onClick={() => setShowSearch(!showSearch)}><AiOutlineSearch /></button>
            {showSearch ?
                <div className='form-group pt-1'>
                    <input className='form-control text-center' type='text' value={filterValue || ''} onChange={e => setFilter(e.target.value)} />
                </div>
            : ''
            }
        </div>
    )
}
