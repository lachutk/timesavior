import React, { useMemo, useState, useEffect } from 'react';
import { useTable, useSortBy, useFilters, usePagination } from 'react-table';
import { BsFillCaretUpFill, BsFillCaretDownFill } from "react-icons/bs";
import { ColumnFilter } from './ColumnFilter';
import { format } from 'date-fns';
import axios from 'axios';
import Cookies from 'js-cookie';
import { FiEdit } from "react-icons/fi";
import { TiTickOutline, TiTick } from "react-icons/ti";
import { MdNoteAdd } from "react-icons/md";
import ModalEditTask from './ModalEditTask';
import ModalAddNote from './ModalAddNote';
import { connect } from 'react-redux';

const TaskTable = ({ email_global, Tasks, setTasks, taskTags, setTaskTags, Loading, setLoading }) => {
    const [taskCheckboxes, setTaskCheckboxes] = useState([]);
    const [readyCheckboxes, setReadyCheckboxes] = useState(false);
    const [editTaskShow, setEditTaskShow] = useState(false);
    const [editedTaskOption, setEditedTaskOption] = useState('');
    const [addNoteShow, setAddNoteShow] = useState(false);
    const [addNoteOption, setAddNoteOption] = useState('');
    const [loadingTaskToArray, setLoadingTaskToArray] = useState(true);
    let taskArray = []
    const getTasks = () => {
        setLoadingTaskToArray(true);
        taskArray = []
        Tasks.map((task) => {  
            taskArray.push(
                {
                    id: task.id,
                    notification: task.notification,
                    done: task.done,
                    edit: false,
                    toDelete: false
                }
            )
        })
        if(taskArray.length === Tasks.length) {
            setLoadingTaskToArray(false)
            setTaskCheckboxes(taskArray);
            }
    }
    useEffect(() => {
        if(taskCheckboxes.length === Tasks.length)
            setReadyCheckboxes(true);
    }, [taskCheckboxes])
    useEffect(() => {
        getTasks();
    }, [Tasks])
    
    const columns = useMemo(() => {
        const columns = [
            {
                Header: 'Title',
                accessor: 'title',
            },
            {
                Header: 'Tag name',
                accessor: 'task_tag',
            },
            {
                Header: 'Date',
                accessor: 'date',
                Cell: ({ value }) => { return value ? format(new Date(value), 'dd/MM/yyyy') : '' },
            },
            {
                Header: 'Periodicity',
                accessor: 'periodicity',
                Cell: ({ value }) => {
                    let days = '';
                    if(value){
                        if(typeof(value) != 'object') {
                            value = value.replace(/'/g,'"');
                            value = JSON.parse(value);
                        }
                        var i;
                        for(i in value)
                        {
                            if(value[i] == 1)
                                days += i + ', ';
                        }
                    }
                    return days;
                }
            },
            {
                Header: 'Importance',
                accessor: 'importance',
            },
            {
                Header: 'Notification',
                accessor: 'notification',
                Cell: () => {
                    return (null);}
            },
            {
                Header: 'Done',
                accessor: 'done',
                Cell: () => {
                    return (null);}
            }
        ];
        return columns;
    }, [Tasks])
    const data = useMemo(() => Tasks, [Tasks])

    const defaultColumn = useMemo(() => {
        return {
            Filter: ColumnFilter
        }
    }, [])

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        nextPage,
        previousPage,
        canNextPage,
        canPreviousPage,
        pageOptions,
        gotoPage,
        pageCount,
        setPageSize,
        prepareRow,
        state,
    } = useTable({
        columns,
        data,
        defaultColumn,
        initialState: { pageSize: 10 }
    },
        useSortBy,
        useFilters,
        usePagination
    );

    const onChangeCheckbox = (name, index) => {
        let editCheckbox = [...taskCheckboxes];
        editCheckbox[index][name] = (!editCheckbox[index][name])
        if(editCheckbox[index].id == Tasks[index].id)
        {
            let not;
            if (editCheckbox[index].notification)
                not = 1;
            else
                not = 0;
            let don;
            if (editCheckbox[index].done)
                don = 1;
            else
                don = 0;

            const body = JSON.stringify({
                'withCredentials': true,
                id: editCheckbox[index].id,
                title: Tasks[index].title,
                importance: Tasks[index].importance,
                notification: not,
                done: don,
                task_tag: Tasks[index].task_tag,
                date: Tasks[index].date,
                periodicity: Tasks[index].periodicity,
            });
            const editConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `JWT ${localStorage.getItem('access')}`,
                    'Accept': 'application/json',
                    'X-CSRFToken': Cookies.get('csrftoken')
                }
            };
            axios.put(`${process.env.REACT_APP_API_URL}/profile/task`, body, editConfig)
                .then(res => {
                    console.log(res.data);
                    setTaskCheckboxes(editCheckbox);
                    let newTasks = [...Tasks];
                    newTasks[index] = {
                        id: editCheckbox[index].id,
                        title: Tasks[index].title,
                        importance: Tasks[index].importance,
                        notification: editCheckbox[index].notification,
                        done: editCheckbox[index].done,
                        task_tag: Tasks[index].task_tag,
                        date: Tasks[index].date,
                        periodicity: Tasks[index].periodicity,
                    };
                    setTasks(newTasks);
                }).catch(err => {
                    console.log(err)
                })
        }
    }

    const deleteTask = (id) => {
        const body = JSON.stringify({
            'withCredentials': true,
            task_id: id,
        });
        const deleteConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        deleteConfig.data = body;
        axios.delete(`${process.env.REACT_APP_API_URL}/profile/task`, deleteConfig)
            .then(res => {
                console.log(res.data);
                const newTasks = Tasks.filter(task => task.id !== id);
                setTasks(newTasks);
            }).catch(err => {
                console.log(err)
            })
    };

    const popUpEditTask = (id) => {
        setEditTaskShow(!editTaskShow);
        setEditedTaskOption(id);
    };

    const popUpNoteAdd = (id) => {
        setAddNoteShow(!addNoteShow);
        setAddNoteOption(id);
    }

    const { pageIndex, pageSize } = state
    return (
        <>  
            {readyCheckboxes && (editedTaskOption != '' || editedTaskOption === 0) && Tasks != [] ?
            <ModalEditTask 
                editTaskShow={editTaskShow} setEditTaskShow={setEditTaskShow} 
                editedTaskOption={editedTaskOption} setEditedTaskOption={setEditedTaskOption} 
                setTasks={setTasks} Tasks={Tasks}
                taskTags={taskTags}
                setTaskTags={setTaskTags}
                Loading={Loading}
                setLoading={setLoading}
                email_global={email_global}
            />
            : '' }
            {addNoteOption != '' ?
            <ModalAddNote 
                email_global={email_global}
                addNoteShow={addNoteShow}
                setAddNoteShow={setAddNoteShow}
                addNoteOption={addNoteOption}
                setAddNoteOption={setAddNoteOption}
            />
            : ''}
            <table className='task_table mt-3' {...getTableProps()}>
                <thead>
                    {
                        headerGroups.map((headerGroup) => (
                            <tr {...headerGroup.getHeaderGroupProps()}>
                                {
                                    headerGroup.headers.map((column) => {
                                        if(column.Header != headerGroup.headers[5].Header  && column.Header != headerGroup.headers[6].Header)
                                            return(
                                                <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                                                    {column.render('Header')}
                                                    <span>{column.canFilter ? column.render('Filter') : null}</span>
                                                    <span>
                                                        {column.isSorted ? (column.isSortedDesc ? <BsFillCaretDownFill /> : <BsFillCaretUpFill />) : ''}
                                                    </span>
                                                </th>
                                            )
                                    })
                                }
                                <th colspan="4">Operations</th>
                            </tr>
                        ))}
                </thead> 
                <tbody {...getTableBodyProps()}>
                    { readyCheckboxes ?
                        page.map((row, i) => {
                            prepareRow(row);
                            return (
                                <tr className={row.cells[6].value === true ? 'strikeout importance__' + row.cells[4].value : '' + 'importance__' + row.cells[4].value} {...row.getRowProps()}>
                                    {
                                        row.cells.map((cell) => {
                                            if(cell.value != row.cells[5].value  && cell.value != row.cells[6].value)
                                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                            else if(cell.column.Header == "Importance")
                                                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                                        })                                        
                                    }
                                    {taskCheckboxes.map((checkbox, index) => {
                                        if(checkbox.id == row.cells[5].row.original.id || checkbox.id == row.cells[6].row.original.id)
                                        {
                                            return (
                                                <>
                                                    {/* <td>
                                                        {checkbox.notification ?
                                                            <button type="button" title="Notification" className="close form-button" onClick={() => onChangeCheckbox('notification', index)}>
                                                                    <IoIosNotifications />
                                                            </button>
                                                        :   <button type="button" title="Notification" className="close form-button" onClick={() => onChangeCheckbox('notification', index)}>
                                                                    <IoIosNotificationsOff />
                                                            </button> }
                                                    </td> */}
                                                    <td>
                                                        {checkbox.done ?
                                                            <button type="button" title="Done" className="close form-button" onClick={() => onChangeCheckbox('done', index)}>
                                                                <TiTick />
                                                            </button>
                                                        :   <button type="button" title="Done" className="close form-button" onClick={() => onChangeCheckbox('done', index)}>
                                                                <TiTickOutline />
                                                            </button> }
                                                    </td>
                                                    <td>
                                                        <button type="button" title="Edit task" className="close form-button" onClick={() => popUpEditTask(index)}>
                                                            <FiEdit />
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" title="Add note" className="close form-button" onClick={() => popUpNoteAdd(Tasks[index].id)}>
                                                            <MdNoteAdd />
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" title="Delete task" className="close form-button" onClick={() => deleteTask(checkbox.id)}>
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </td>
                                                </>
                                            )
                                        }

                                        })}
                                    
                                </tr>
                            )
                        })
                        : ''
                    }
                </tbody>
            </table>
            <form className='form-inline justify-content-center mt-3'>
                <span>
                    Page{' '}
                    <strong>
                        {pageIndex + 1} of {pageOptions.length}
                    </strong>{' '}
                </span>
                <div className="form-group ml-1">
                    <label class="control-label">| Go to page: {' '}</label>
                    <input type='number' className='form-control ml-1' defaultValue={pageIndex + 1}
                        onChange={e => {
                            const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                            gotoPage(pageNumber)
                        }}
                        style={{ width: '50px' }}
                    />{' '}
                </div>
                <div className="form-group">
                    <select className='form-control ml-1' value={pageSize} onChange={e => setPageSize(Number(e.target.value))}>
                        {
                            [10, 25, 50].map(pageSize => (
                                <option key={pageSize} value={pageSize}>
                                    Show {pageSize}
                                </option>
                            ))
                        }
                    </select>
                </div>
            </form>
            <div className='mt-2 text-center'>
                <button type="button" title="First" class="btn btn-default" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>{'<<'}</button>
                <button type="button" title="Previous" class="btn btn-default" onClick={() => previousPage()} disabled={!canPreviousPage}>Previous</button>
                <button type="button" title="Next" class="btn btn-default" onClick={() => nextPage()} disabled={!canNextPage}>Next</button>
                <button type="button" title="Last" class="btn btn-default" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>{'>>'}</button>
            </div>
        </>
    );
}

const mapStateToProps = state => ({
    email_global: state.profile.email,

});
export default connect(mapStateToProps, )(TaskTable);