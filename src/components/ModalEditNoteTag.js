import { Modal, Button, InputGroup, FormControl } from 'react-bootstrap';
import React from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

const ModalEditNoteTag = ({ editedOption, setEditedOption, editNoteTagShow, setEditNoteTagShow, setSelectedOption, noteTags, setNoteTags }) => {

    const popUpEditTag = (value, label) => {
        setEditNoteTagShow(!editNoteTagShow);
        setEditedOption({ value: value, label: label });
    };

    const editNoteTagToBackend = () => {
        console.group('Edited option and to what edit');
        console.log(editedOption);
        console.groupEnd();
        const body = JSON.stringify({
            'withCredentials': true,
            tag_id: editedOption.value,
            tag_name: editedOption.label
        });
        const editConfig = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        axios.put(`${process.env.REACT_APP_API_URL}/profile/note-tag`, body, editConfig)
            .then(res => {
                setSelectedOption({ value: editedOption.value, label: editedOption.label });
                let newNoteTags = [...noteTags];
                let thisTag = newNoteTags.find((taskTag) => taskTag.id === editedOption.value);
                let index = newNoteTags.findIndex((taskTag) => taskTag.id === editedOption.value);
                newNoteTags[index] = {
                    id: editedOption.value,
                    tag_name: editedOption.label,
                    share: thisTag.share,
                    user_id: thisTag.user_id
                };
                setNoteTags(newNoteTags);
                setEditNoteTagShow(!editNoteTagShow);
            }).catch(err => {
                console.log(err)
                setEditNoteTagShow(!editNoteTagShow);
            })
    };

    return (
        <Modal show={editNoteTagShow} onHide={popUpEditTag}>

            <Modal.Header closeButton>
                <Modal.Title>Change note tag name</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                    <InputGroup.Text id="inputGroup-sizing-default">Note tag name</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        aria-label="Default"
                        aria-describedby="inputGroup-sizing-default"
                        defaultValue={editedOption.label}
                        onChange={e => setEditedOption({ value: editedOption.value, label: e.target.value })} 
                        required    
                    />
                </InputGroup>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={popUpEditTag}>Close</Button>
                <Button variant="primary" onClick={() => editNoteTagToBackend()}>Ok</Button>
            </Modal.Footer>

        </Modal>
    );
}

export default ModalEditNoteTag;