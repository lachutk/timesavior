import {
    USER_LOADED_SUCCESS,
    USER_LOADED_FAIL,
    FACEBOOK_AUTH_FAIL,
    GOOGLE_AUTH_FAIL,
    LOGIN_FAIL,
    LOGOUT,
    SIGNUP_FAIL,
    DELETE_USER_SUCCESS,
    UPDATE_USER_PROFILE_SUCCESS,
    UPDATE_USER_PROFILE_FAIL,
} from '../actions/types';

const initialState = {
    username: '',
    first_name: '',
    last_name: '',
    email: '',
};
 
export default function auth_process(state = initialState, action) {
    const { type, payload } = action;

    switch (type) {
        case UPDATE_USER_PROFILE_SUCCESS:
        case USER_LOADED_SUCCESS:
            return {
                ...state,
                username: payload.username,
                first_name: payload.profile.first_name,
                last_name: payload.profile.last_name,
                email: payload.profile.email
            } 
        case USER_LOADED_FAIL:
            return {
                ...state,
                username: '',
                first_name: '',
                last_name: '',
                email: '',
            }
        case FACEBOOK_AUTH_FAIL:
        case GOOGLE_AUTH_FAIL:
        case LOGIN_FAIL:
        case LOGOUT:
        case SIGNUP_FAIL:
        case DELETE_USER_SUCCESS:
            return {
                ...state,
                username: '',
                first_name: '',
                last_name: '',
                email: '',
                    }
        case UPDATE_USER_PROFILE_FAIL:
            return {
                ...state,
                error: payload.error
            }
        default:
            return state
    }
};