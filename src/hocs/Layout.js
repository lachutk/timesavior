import React, { useEffect } from 'react';
import NavbarMenu from '../components/Navbar';
import { connect } from 'react-redux';
import { checkAuthenticated } from '../actions/auth';
import { load_us } from '../actions/profile';

const Layout = ({ checkAuthenticated, load_us, children }) => {
    useEffect(() => {
        checkAuthenticated();
        load_us();
    }, []);

    return (
        <div>
            <NavbarMenu />
            {children}
        </div>
    );
};

export default connect(null, { checkAuthenticated, load_us })(Layout);