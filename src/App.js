import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './containers/Home';
import Login from './containers/Login';
import Activate from './containers/Activate';
import ResetPassword from './containers/ResetPassword';
import ResetPasswordConfirm from './containers/ResetPasswordConfirm';
import SignUp from './containers/SignUp';
import Layout from './hocs/Layout';
import Facebook from './containers/Facebook';
import Google from './containers/Google';
import Tasks from './containers/Tasks';
import Notes from './containers/Notes';
import Friends from './containers/Friends';
import Profile from './containers/Profile';
import Pomodoro from './containers/Pomodoro';
import { Provider } from 'react-redux';
import store from './Store';
import PrivateRoute from './hocs/PrivateRoute';
import NotFound from './components/NotFound';
import './sass/main.scss';
import axios from 'axios';

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken' 

const App = () => (
    <Provider store={store}>
        <Router>
            <Layout>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/signup' component={SignUp} />
                    <Route exact path='/facebook' component={Facebook} />
                    <Route exact path='/google' component={Google} />
                    <PrivateRoute exact path='/tasks' component={Tasks} />
                    <PrivateRoute exact path='/notes' component={Notes} />
                    <PrivateRoute exact path='/friends' component={Friends} />
                    <PrivateRoute exact path='/profile' component={Profile} />
                    <PrivateRoute exact path='/pomodoro' component={Pomodoro} />
                    <Route exact path='/activate/:uid/:token' component={Activate} />
                    <Route exact path='/reset_password' component={ResetPassword} />
                    <Route exact path='/password/reset/confirm/:uid/:token' component={ResetPasswordConfirm} />
                    <Route component={NotFound} />
                </Switch>
            </Layout>
        </Router>
    </Provider>
);

export default App;