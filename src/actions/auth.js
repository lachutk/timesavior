import {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    AUTHENTICATED_SUCCESS,
    AUTHENTICATED_FAIL,
    LOGOUT,
    SIGNUP_SUCCESS,
    SIGNUP_FAIL,
    ACTIVATION_SUCCESS,
    ACTIVATION_FAIL,
    PASSWORD_RESET_FAIL,
    PASSWORD_RESET_SUCCESS,
    PASSWORD_RESET_CONFIRM_FAIL,
    PASSWORD_RESET_CONFIRM_SUCCESS,
    GOOGLE_AUTH_SUCCESS,
    GOOGLE_AUTH_FAIL,
    FACEBOOK_AUTH_FAIL,
    FACEBOOK_AUTH_SUCCESS,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAIL,
} from './types';
import axios from 'axios';
import { load_us } from './profile';
import Cookies from 'js-cookie';
import { setAlert } from './alert';

export const googleAuthenticate = (state, code) => async dispatch => {
    if (state && code && !localStorage.getItem('access')) {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        const details = {
            'state': state,
            'code': code
        };
        const formBody = Object.keys(details).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(details[key])).join('&');

        try {
            const res = await axios.post(`${process.env.REACT_APP_API_URL}/auth/o/google-oauth2/?${formBody}`, config);
            
            dispatch({
                type: GOOGLE_AUTH_SUCCESS,
                payload: res.data
            });

            dispatch(load_us());
        } catch (err) {
            dispatch({
                type: GOOGLE_AUTH_FAIL
            });
            dispatch(setAlert('Google Authenticate Failed', 'error'));
        }
    }
};

export const facebookAuthenticate = (state, code) => async dispatch => {
    if (state && code && !localStorage.getItem('access')) {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        const details = {
            'state': state,
            'code': code
        };
        const formBody = Object.keys(details).map(key => encodeURIComponent(key) + '=' + encodeURIComponent(details[key])).join('&');

        try {
            const res = await axios.post(`${process.env.REACT_APP_API_URL}/auth/o/facebook/?${formBody}`, config);
            
            dispatch({
                type: FACEBOOK_AUTH_SUCCESS,
                payload: res.data
            });

            dispatch(load_us());
        } catch (err) {
            dispatch({
                type: FACEBOOK_AUTH_FAIL
            });
            dispatch(setAlert('Facebook Authenticate Failed', 'error'));
        }
    }
};
export const checkAuthenticated = () => async dispatch => {
    if (localStorage.getItem('access')) {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        };

        const body = JSON.stringify({ token: localStorage.getItem('access') });

        try {
            const res = await axios.post(`${process.env.REACT_APP_API_URL}/auth/jwt/verify/`, body, config)

            if (res.data.code !== 'token_not_valid') {
                dispatch({
                    type: AUTHENTICATED_SUCCESS
                }); 
                dispatch(setAlert('Verification Success', 'success'));
            } else {
                dispatch({
                    type: AUTHENTICATED_FAIL
                });
            }
        } catch (err) {
            dispatch({
                type: AUTHENTICATED_FAIL
            });
        }

    } else {
        dispatch({
            type: AUTHENTICATED_FAIL
        });
    }
};

export const login = (email, password) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const body = JSON.stringify({ email, password });

    try {
        const res = await axios.post(`${process.env.REACT_APP_API_URL}/auth/jwt/create/`, body, config);
        dispatch({
            type: LOGIN_SUCCESS,
            payload: res.data
        });

        dispatch(load_us());
    } catch (err) {
        dispatch({
            type: LOGIN_FAIL
        });
        dispatch(setAlert('Invalid email or password', 'error'));
    }
};

export const reset_password = (email) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };

    const body = JSON.stringify({ email });

    try {
        await axios.post(`${process.env.REACT_APP_API_URL}/auth/users/reset_password/`, body, config);

        dispatch({
            type: PASSWORD_RESET_SUCCESS
        });
        dispatch(setAlert('Email for reset password will be send', 'success'));
    } catch (err) {
        dispatch({
            type: PASSWORD_RESET_FAIL
        });
        dispatch(setAlert('Wrong email, try again', 'error'));
    }
};

export const reset_password_confirm = (uid, token, new_password, re_new_password) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const body = JSON.stringify({ uid, token, new_password, re_new_password });

    try {
        await axios.post(`${process.env.REACT_APP_API_URL}/auth/users/reset_password_confirm/`, body, config);

        dispatch({
            type: PASSWORD_RESET_CONFIRM_SUCCESS
        });
        dispatch(setAlert('Reset password success, login now!', 'success'));
    } catch (err) {
        dispatch({
            type: PASSWORD_RESET_CONFIRM_FAIL
        });
        dispatch(setAlert('Passwords incorrect, try again', 'error'));
    }
};
export const logout = () => dispatch => {
    dispatch({
        type: LOGOUT
    });
    dispatch(setAlert('Logout successful', 'success'));
};

export const signup = ({ first_name, last_name, email, password, re_password }) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const body = JSON.stringify({ first_name, last_name, email, password, re_password }); 

    try {
        const res = await axios.post(`${process.env.REACT_APP_API_URL}/auth/users/`, body, config);

        dispatch({
            type: SIGNUP_SUCCESS,
            payload: res.data
        });
        dispatch(setAlert('Your account is created! Now you must confirm email', 'success'));
    } catch (err) {
        dispatch({
            type: SIGNUP_FAIL
        });
        console.log(err)
        console.log(body)
        dispatch(setAlert('Sign up failed. Check your email or contact with administrator', 'error')); 
    }
};

export const verify = (uid, token) => async dispatch => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    }

    const body = JSON.stringify({ uid, token });

    try {
        const res = await axios.post(`${process.env.REACT_APP_API_URL}/auth/users/activation/`, body, config);
        dispatch({
            type: ACTIVATION_SUCCESS,
            payload: res.data
        });
        dispatch(setAlert('Your account is activated', 'success'));

    } catch (err) {
        dispatch({
            type: ACTIVATION_FAIL
        });
        dispatch(setAlert('Activation failed', 'error'));
    }
};

export const delete_account = (current_password) => async dispatch => {
    const body = JSON.stringify({
        'withCredentials': true,
        current_password 
    });
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `JWT ${localStorage.getItem('access')}`,
            'Accept': 'application/json',
            'X-CSRFToken': Cookies.get('csrftoken')
        }
    };
    config.data = body;

    try {
        const res = await axios.delete(`${process.env.REACT_APP_API_URL}/accounts/delete`, config);
        if (res.data.success) {
            dispatch({
                type: DELETE_USER_SUCCESS,
                payload: res.data
            });
            dispatch(setAlert('Your account is deleted', 'success'));
        } else {
            dispatch({
                type: DELETE_USER_FAIL,
                payload: res.data
            }); 
            dispatch(setAlert('Invalid password', 'error'));
        }
    } catch (err) {
        dispatch({
            type: DELETE_USER_FAIL,
            payload: { error: "Error in Axios" }
        });
        dispatch(setAlert('Invalid password', 'error'));
    }
}