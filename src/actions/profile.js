import Cookies from 'js-cookie';
import axios from 'axios'; 
import {
    USER_LOADED_SUCCESS,
    USER_LOADED_FAIL,
    UPDATE_USER_PROFILE_SUCCESS,
    UPDATE_USER_PROFILE_FAIL,
} from './types';
import { setAlert } from './alert'; 

export const load_us = () => async dispatch => {
    if (localStorage.getItem('access')) {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`, 
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
        const body_1 = JSON.stringify({
            'withCredentials': true,
            tag_name: 'School',
            share: 0
        });
        const body_2 = JSON.stringify({
            'withCredentials': true,
            tag_name: 'Work',
            share: 0
        });
        const body_3 = JSON.stringify({
            'withCredentials': true,
            tag_name: 'Hobby',
            share: 0
        });

        try {
            await axios.get(`${process.env.REACT_APP_API_URL}/profile/task-tag`, config)
            .then(res => {
                if(res.data.task_tags.length == 0){
                    axios.post(`${process.env.REACT_APP_API_URL}/profile/task-tag`, body_1, config)
                    axios.post(`${process.env.REACT_APP_API_URL}/profile/task-tag`, body_2, config)
                    axios.post(`${process.env.REACT_APP_API_URL}/profile/task-tag`, body_3, config)
                }
            }).catch(err => {})
            const res = await axios.get(`${process.env.REACT_APP_API_URL}/accounts/user`, config);
            dispatch({
                type: USER_LOADED_SUCCESS,
                payload: res.data
            });
        } catch (err) {
            dispatch({
                type: USER_LOADED_FAIL
            })
        }
    } else {
        dispatch({
            type: USER_LOADED_FAIL
        })
    }
};

export const update_profile = (first_name, last_name, email) => async dispatch => {
    if (localStorage.getItem('access')) {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `JWT ${localStorage.getItem('access')}`,
                'Accept': 'application/json',
                'X-CSRFToken': Cookies.get('csrftoken')
            }
        };
       
        const body = JSON.stringify({
            'withCredentials': true,
            first_name,
            last_name,
            email
        });
        try {
            const res = await axios.put(`${process.env.REACT_APP_API_URL}/accounts/update`, body, config);

            if (res.data.profile && res.data.username) {
                dispatch({
                    type: UPDATE_USER_PROFILE_SUCCESS,
                    payload: res.data
                });
                dispatch(setAlert('Account update is success', 'success'));
            }
            else {
                dispatch({
                    type: UPDATE_USER_PROFILE_FAIL,
                    payload: res.data
                });
                dispatch(setAlert('Account update is failed', 'error'));
            }
        } catch (err) {
            dispatch({
                type: UPDATE_USER_PROFILE_FAIL,
                payload: { error: "Error in Axios" }
            });
            dispatch(setAlert('Account update is failed', 'error'));
        }
    }
    else {
        dispatch({
            type: UPDATE_USER_PROFILE_FAIL,
            payload: { error: "You don't have permissions" }
        });
        dispatch(setAlert('Account update is failed', 'error'));
    }
}